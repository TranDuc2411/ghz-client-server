import { CommonProvider } from "@core/common/CommonContext";
import AppRoutes from "./AppRoutes";

function App() {
  return (
    <CommonProvider>
      <AppRoutes />
    </CommonProvider>
  );
}

export default App;
