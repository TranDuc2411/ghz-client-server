import A0003Content from "@components/A00.common/A00.03.not-found/A0003Content";
import { A00Router } from "@container/A00.common/A00Common";
import { A02Router } from "@container/A02.warehouse-management/A02Router";
import { A03Router } from "@container/A03.config-management/A03Router";
import { A04Router } from "@container/A04.post-mangement/A04Router";
import { A05Router } from "@container/A05.client-management/A05Router";
import { A06Router } from "@container/A06.accesseoriess-management/A06Router";
import { A07Router } from "@container/A07.transaction-management/A07Router";
import LoginLayout from "@layout/login-layout/LoginLayout";
import { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { A01Router } from "../container/A01.admin-management/A01Router";
import FunctionLayout from "../layout/function-layout/FunctionLayout";

export default function AppRoutes() {
  const router = [
    ...A00Router,
    ...A01Router,
    ...A02Router,
    ...A03Router,
    ...A04Router,
    ...A05Router,
    ...A06Router,
    ...A07Router,
  ];
  return (
    <Suspense
      fallback={
        <div className="w-screen h-screen flex justify-center items-center text-blue-400">
          Đang tải...
        </div>
      }
    >
      <Routes>
        {router.map((item, index) => {
          return (
            <Route
              key={index}
              path={item?.path}
              element={LayoutRender(item?.element, item?.layout)}
            />
          );
        })}
        <Route path="*" element={<A0003Content />} />
      </Routes>
    </Suspense>
  );
}

const LayoutRender = (element, layout) => {
  switch (layout) {
    case "function":
      return <FunctionLayout>{element}</FunctionLayout>;

    case "login":
      return <LoginLayout>{element}</LoginLayout>;

    default:
      return element;
  }
};
// NOte. Muốn dùng Router lồng vào nhâu thì cần sử dung "Outlet" của "react-router-dom" để render
