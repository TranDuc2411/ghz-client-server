export const InputRequired = [
  {
    required: true,
    whitespace: true,
    message: "Trường thông tin không được để trống!",
  },
];
export const InputNumberRequired = [
  {
    required: true,
    message: "Trường thông tin không được để trống!",
  },
];
export const InputMax = [
  {
    max: 200,
    message: "Trường thông tin không được vượt quá 200 ký tự!",
  },
];

export const InputTextAreaMax = [
  {
    max: 1000,
    message: "Trường thông tin không được vượt quá 1000 ký tự!",
  },
];
