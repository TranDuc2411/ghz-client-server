import { Tag } from "antd";
import _ from "lodash";

export function configBgColor(color) {
  switch (color?.toUpperCase()) {
    case "#5DA0F6":
      return "bg-blue-5";
    case "#FFA940":
      return "bg-orange-5";
    case "#FF4D4F":
      return "bg-red-5";
    case "#4DD077":
      return "bg-green-5";
    case "#BFBFBF":
      return "bg-grey-6";
    case "#36CFC9":
      return "bg-cyan-5";
    case "#FFD700":
      return "bg-yellow-1";
    case "#EE82EE":
      return "bg-violet-1";
    default:
      return "bg-transparent";
  }
}
export const STICKER_LIST = [
  "default",
  "blue",
  "green",
  "cyan",
  "red",
  "geekblue",
  "gold",
  "lime",
  "magenta",
  "orange",
  "pink",
  "purple",
  "volcano",
  "yellow",
];
export const STICKER_OPTION = [
  { label: <Tag color="default">Trắng xám</Tag>, value: "default" },
  { label: <Tag color="blue">Xanh da trời</Tag>, value: "blue" },
  { label: <Tag color="green">Xanh lá</Tag>, value: "green" },
  { label: <Tag color="cyan">Lục lam</Tag>, value: "cyan" },
  { label: <Tag color="red">Đỏ</Tag>, value: "red" },
  { label: <Tag color="geekblue">Xanh geek</Tag>, value: "geekblue" },
  { label: <Tag color="gold">Vàng Kim</Tag>, value: "gold" },
  { label: <Tag color="lime">Vàng chanh</Tag>, value: "lime" },
  { label: <Tag color="magenta">Đỏ tía</Tag>, value: "magenta" },
  { label: <Tag color="orange">Cam</Tag>, value: "orange" },
  { label: <Tag color="pink">Hồng</Tag>, value: "pink" },
  { label: <Tag color="purple">Tím</Tag>, value: "purple" },
  { label: <Tag color="volcano">Đỏ núi lửa</Tag>, value: "volcano" },
  { label: <Tag color="yellow">Vàng</Tag>, value: "yellow" },
];
export function formatCurrency(
  amount = 0,
  decimalCount = 0,
  decimal = ".",
  thousands = ","
) {
  try {
    let currency = amount;
    // Parse sang số có số lượng hàng thập phân bằng 'decimalCount'
    currency = _.round(currency, decimalCount);

    // Parse sang format tiền
    currency = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    })
      .format(amount)
      .replaceAll(/(\$)+|(\.00)+/g, "");

    // Thay dấu hàng nghìn
    currency = _.join(
      _.map(currency.split(","), (item) => {
        // Thay dấu hàng thập phân
        if (_.includes(item, ".")) {
          return item.replace(".", decimal);
        }
        return item;
      }),
      thousands
    );

    return currency;
  } catch (e) {
    console.error(e);
    return amount;
  }
}
export function getScreenWidth() {
  const deviceWidth = window.innerWidth;
  if (deviceWidth > 1024) {
    return "pc";
  } else if (deviceWidth <= 1024 && deviceWidth >= 768) {
    return "table";
  } else {
    return "mobile";
  }
}

export function isSuperAdmin() {
  const role = localStorage.getItem("role");
  if (role == 1) {
    return true;
  }
  return false;
}

