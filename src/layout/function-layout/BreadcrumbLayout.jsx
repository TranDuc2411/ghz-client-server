import { getScreenWidth } from "@common/utils";
import _ from "lodash";

export default function BreadcrumbLayout() {
  const SiderMenuItem = [
    {
      key: "0",
      label: "Thông tin tài khoản",
      path: "/user-info",
    },
    {
      key: "1",
      label: "Quản lý tài khoản",
      path: "/admin-management",
    },
    {
      key: "21",
      label: "Quản lý kho / Quản lý sản phẩm",
      path: "/warehouse-management",
    },
    {
      key: "22",
      label: "Quản lý kho / Quản lý phụ kiện",
      path: "/accesseoriess-management",
    },
    {
      key: "31",
      label: "Quản lý cấu hình / Quản lý trạng thái",
      path: "/config-status-management",
    },
    {
      key: "32",
      label: "Quản lý cấu hình / Quản lý nhãn dán",
      path: "/config-stickers-management",
    },
    {
      key: "33",
      label: "Quản lý cấu hình / Quản lý tên sản phẩm",
      path: "/product-name-management",
    },
    {
      key: "4",
      label: "Quản lý khách hàng",
      path: "/client-management",
    },
    {
      key: "7",
      label: "Quản lý giao dịch",
      path: "/transaction-management",
    },
  ];

  const ScreenType = [
    {
      label: "Danh sách",
      key: "",
    },
    {
      label: "Tạo mới",
      key: "create",
    },
    {
      label: "Chỉnh sửa",
      key: "update",
    },
    {
      label: "Xem chi tiết",
      key: "info",
    },
  ];

  function getScreenType() {
    let result = "";
    _.map(ScreenType, (item) => {
      if (_.includes(location.pathname, item.key)) {
        result = item.label;
      }
    });
    return result;
  }
  function getFunctionName() {
    let result = "";
    _.map(SiderMenuItem, (item) => {
      if (_.includes(location.pathname, item.path)) {
        result = item.label + " / ";
      }
    });
    return result;
  }
  return (
    <div>
      <span
        className={`${
          getScreenWidth() == "pc" ? "text-base" : "text-sm"
        } font-bold`}
      >
        {getFunctionName()}
      </span>
      <span
        className={`${
          getScreenWidth() == "pc" ? "text-sm" : "text-[12px]"
        } italic text-gray-500`}
      >
        {getScreenType()}
      </span>
    </div>
  );
}
