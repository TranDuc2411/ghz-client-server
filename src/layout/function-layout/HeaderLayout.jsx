import { UserOutlined } from "@ant-design/icons";
import logo from "@assets/logo.png";
import { Avatar, Dropdown, Typography } from "antd";
import { Header } from "antd/es/layout/layout";
import { AiOutlineLogout } from "react-icons/ai";
import { IoMdMenu } from "react-icons/io";
import { useNavigate } from "react-router-dom";
import BreadcrumbLayout from "./BreadcrumbLayout";

export default function HeaderLayout({ collapsed, setCollapsed }) {
  const navigate = useNavigate();
  const deviceWidth = window.innerWidth;

  function logOut() {
    localStorage.clear();
    navigate("/");
  }
  const items = [
    {
      key: "1",
      label: (
        <Typography.Link onClick={() => navigate("/user-info")}>
          <UserOutlined />
          <span className="ml-2">Thông tin tài khoản</span>
        </Typography.Link>
      ),
    },
    {
      key: "2",
      label: (
        <Typography.Link className="text-red-500" onClick={logOut}>
          <AiOutlineLogout />
          <span className="ml-2">Đăng xuất</span>
        </Typography.Link>
      ),
    },
  ];

  const fullName = localStorage.getItem("fullName") || "Lý Mạnh Huy";
  return (
    <Header className="flex justify-between items-center bg-white h-[56px] border-l border-solid border-grey-3 w-auto px-4">
      {deviceWidth <= 1024 ? (
        <div className="h-[80%] flex justify-center items-center">
          <div
            className="flex justify-center items-center px-4"
            onClick={() => setCollapsed(false)}
          >
            <IoMdMenu size={28} />
          </div>
          <img src={logo} className={`cursor-pointer duration-500 h-8 w-8`} />
        </div>
      ) : (
        <div className="breadcrumb">
          <BreadcrumbLayout />
        </div>
      )}

      <Dropdown
        menu={{
          items,
        }}
        placement="bottomLeft"
      >
        <div className="h-[80%] w-max-[10%] flex justify-center items-center rounded-md px-4 hover:bg-gray-100 ">
          <Avatar
            style={{ backgroundColor: "#87d068" }}
            icon={<UserOutlined />}
          />
          <span className="ml-2 text-gray-500 font-mono">{fullName}</span>
        </div>
      </Dropdown>
    </Header>
  );
}
