import { Drawer, Layout } from "antd";
import { useState } from "react";
import BreadcrumbLayout from "./BreadcrumbLayout";
import HeaderLayout from "./HeaderLayout";
import SiderMenu from "./SiderMenu";
import "./style.scss";

const { Footer, Content } = Layout;

export default function FunctionLayout({ children }) {
  const deviceWidth = window.innerWidth;
  const [collapsed, setCollapsed] = useState(deviceWidth > 1024 ? false : true);

  return (
    <Layout className="min-h-screen max-h-screen w-screen overflow-x-hidden flex">
      {
        // responsive ẩn khi thiết bị là máy tính
        deviceWidth <= 1024 ? (
          <Drawer
            open={!collapsed}
            placement={"left"}
            closeIcon={null}
            className="w-auto p-0"
            width={320}
            onClose={() => setCollapsed(true)}
          >
            <SiderMenu
              className="max-h-screen w-72 bg-bule-6 flex p-0"
              collapsed={collapsed}
              setCollapsed={setCollapsed}
            />
          </Drawer>
        ) : (
          <div className="h-screen">
            <SiderMenu
              className="max-h-screen w-72 bg-bule-6 flex bg-red-300"
              collapsed={collapsed}
              setCollapsed={setCollapsed}
            />
          </div>
        )
      }
      <Layout className="overflow-y-auto flex">
        <HeaderLayout collapsed={collapsed} setCollapsed={setCollapsed} />
        <Layout className="overflow-x-auto">
          <Content className="p-6 h-full box-border">
            {deviceWidth <= 1024 && (
              <div className="mb-2">
                <BreadcrumbLayout />
              </div>
            )}
            {children}
          </Content>
          <Footer className="text-center">Web created by Koyomi</Footer>
        </Layout>
      </Layout>
    </Layout>
  );
}
