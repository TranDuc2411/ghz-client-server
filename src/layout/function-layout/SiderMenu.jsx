import control from "@assets/control.png";
import logo from "@assets/logo.png";
import { isSuperAdmin } from "@common/utils";
import { Layout, Menu } from "antd";
import { useState } from "react";
import { BsFiletypePdf, BsPostcard } from "react-icons/bs";
import { FaRegHandshake } from "react-icons/fa";
import { FaMoneyBillTrendUp } from "react-icons/fa6";
import { FcCustomerSupport } from "react-icons/fc";
import { GrStatusDisabled, GrTicket, GrUserSettings } from "react-icons/gr";
import { IoSettingsOutline } from "react-icons/io5";
import { LiaWarehouseSolid } from "react-icons/lia";
import { RiBillLine } from "react-icons/ri";
import { useLocation, useNavigate } from "react-router-dom";

export default function SiderMenu({ collapsed, setCollapsed }) {
  const location = useLocation();
  const navigate = useNavigate();
  const deviceWidth = window.innerWidth;

  // sidebar
  const items = [
    getItem("Quản lý kho", "/quan-ly-kho", <LiaWarehouseSolid />, [
      getItem(
        "Sản Phẩm Like NEW",
        "/warehouse-management",
        <FaMoneyBillTrendUp />
      ),
      getItem(
        "Sản Phẩm NEW",
        "/warehouse1-management",
        <FaMoneyBillTrendUp />
      ),
      getItem(
        "Phụ kiện tặng kèm",
        "/accesseoriess-management",
        <FaMoneyBillTrendUp />
      ),
    ]),
    getItem("Quản lý giao dịch", "/transaction-management", <FaMoneyBillTrendUp />),
    getItem("Quản lý bài đăng", "/post-management", <BsPostcard />),
    getItem("Quản lý khách hàng", "/client-management", <FaRegHandshake />),
    isSuperAdmin() &&
      getItem("Quản lý tài khoản", "/admin-management", <GrUserSettings />),
    isSuperAdmin() &&
      getItem("Quản lý cấu hình", "/config-management", <IoSettingsOutline />, [
        getItem(
          "Quản lý trạng thái",
          "/config-status-management",
          <GrStatusDisabled />
        ),
        getItem(
          "Quản lý nhãn dán",
          "/config-stickers-management",
          <GrTicket />
        ),
        getItem(
          "Quản lý tên sản phẩm",
          "/product-name-management",
          <RiBillLine />
        ),
        getItem(
          "Quản lý danh mục sản phẩm",
          "/product-category-management",
          <RiBillLine />
        ),
        getItem("Mẫu hóa đơn", "/mau-hoa-don", <BsFiletypePdf />),
      ]),
    getItem("IT hỗ trợ", "/IT-hỗ-trợ", <FcCustomerSupport />),
  ];

  function getItem(label, key, icon, children, type) {
    return {
      key,
      icon,
      children,
      label,
      type,
    };
  }
  const getLevelKeys = (items1) => {
    const key = {};
    const func = (items2, level = 1) => {
      items2.forEach((item) => {
        if (item.key) {
          key[item.key] = level;
        }
        if (item.children) {
          return func(item.children, level + 1);
        }
      });
    };
    func(items1);
    return key;
  };
  const levelKeys = getLevelKeys(items);
  const [stateOpenKeys, setStateOpenKeys] = useState(["/warehouse-management"]);
  const onOpenChange = (openKeys) => {
    const currentOpenKey = openKeys.find(
      (key) => stateOpenKeys.indexOf(key) === -1
    );
    // open
    if (currentOpenKey !== undefined) {
      const repeatIndex = openKeys
        .filter((key) => key !== currentOpenKey)
        .findIndex((key) => levelKeys[key] === levelKeys[currentOpenKey]);
      setStateOpenKeys(
        openKeys
          // remove repeat key
          .filter((_, index) => index !== repeatIndex)
          // remove current level all child
          .filter((key) => levelKeys[key] <= levelKeys[currentOpenKey])
      );
    } else {
      // close
      setStateOpenKeys(openKeys);
    }
  };

  return (
    <Layout.Sider
      className={`bg-white text-center h-full`}
      width={deviceWidth > 1024 ? "320px" : ""}
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      trigger={null}
    >
      <div className="flex flex-col h-full">
        {deviceWidth > 1024 ? (
          <>
            <img
              src={control}
              className={`absolute cursor-pointer -right-3 top-9 w-7 border-dark-purple border-2 rounded-full  ${
                !collapsed && "rotate-180"
              }`}
              onClick={() => setCollapsed(!collapsed)}
            />
            <div
              className={`${
                collapsed ? "flex justify-center w-full " : ""
              } items-center shadow-[inset_0px_-1px_0px_#f3f3f3] bg-white px-6 py-5`}
            >
              <img
                src={logo}
                className={`cursor-pointer duration-500 ${
                  collapsed ? "rotate-[360deg]" : "w-40 h-40"
                }`}
              />
              {!collapsed && (
                <h1 className={`origin-left font-medium text-xl duration-200 `}>
                  GHz-STORE
                </h1>
              )}
            </div>
          </>
        ) : (
          <div className="h-full w-full flex justity-center items-center shadow-[inset_0px_-1px_0px_#f3f3f3] px-0 pb-2">
            <img src={logo} className={"w-16 h-16 mr-2"} />
            <h1 className={`origin-left font-medium text-xl`}>GHz-STORE</h1>
          </div>
        )}

        <div className="h-auto">
          <Menu
            theme="light"
            mode="inline"
            selectedKeys={[location.pathname]}
            openKeys={stateOpenKeys}
            onOpenChange={onOpenChange}
            items={items}
            onSelect={(e) => navigate(e.key)}
            // style={{
            //   backgroundColor: 'rebeccapurple'
            // }}
          ></Menu>
        </div>
      </div>
    </Layout.Sider>
  );
}
