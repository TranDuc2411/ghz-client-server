import logo from "@assets/logo.png";
import { getScreenWidth } from "@common/utils";
import "./loginlayout.scss";

export default function LoginLayout({ children }) {
  return (
    <div className="body">
      <div className="content">
        <div
          className={`w-full cursor-pointer flex justify-center items-center ${getScreenWidth() == 'pc' ? 'h-48' : 'h-16 p-12'}`}
        >
          <img
            src={logo}
            className={`logo cursor-pointer rotate-0 transition duration-1000 transform hover:rotate-[360deg] ${getScreenWidth() == 'pc' ? 'w-32 h-32 ' : 'w-16 h-16'}`}
          />
        </div>

        <div> {children}</div>
      </div>
    </div>
  );
}
