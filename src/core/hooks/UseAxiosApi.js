import axios from "axios";

const useAxiosAPI = () => {
  const urlApi = import.meta.env.VITE_API_URL;

  const api = axios.create({
    baseURL: urlApi, // Thay bằng URL của backend
  });

  // Thêm accessToken vào headers cho mọi request
  api.interceptors.request.use((config) => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  });
  // Xử lý refresh token khi accessToken hết hạn
  api.interceptors.response.use(
    (response) => response,
    async (error) => {
      const originalRequest = error.config;
      // console.log("duc.tran === ", error.response.status)
      // console.log("duc.tran === ", originalRequest._retry)
      if (error.response.status === 403 && !originalRequest._retry) {
        // error.config._retry: đánh dấu rằng request đó đã được thử lại một lần sau khi làm mới token
        // => tránh một vòng lặp vô tận
        originalRequest._retry = true; // đánh dấu đã gọi  refreshToken
        const refreshToken = localStorage.getItem("refreshToken");
        try {
          const response = await api.post("/api/admin/refresh-token", {
            refreshToken: refreshToken,
          });
          const { accessToken } = response.data;
          localStorage.setItem("accessToken", accessToken);
          api.defaults.headers["Authorization"] = `Bearer ${accessToken}`;
          return api(originalRequest);
        } catch (refreshError) {
          console.error("Phiên đăng nhập hết hạn", refreshError);
          window.location.href = "/";
          return Promise.reject(refreshError);
        }
      }
      if (error.response.status === 401) {
        // error.config._retry: đánh dấu rằng request đó đã được thử lại một lần sau khi làm mới token
        // => tránh một vòng lặp vô tận
        console.log("duc.tran === bạn đã đăng xuất hãy đăng nhập lại")
        window.location.href = "/";
        return Promise.reject(refreshError);
      }
      return Promise.reject(error);
    }
  );

  return api; // Trả về instance của Axios để sử dụng trong các component khác
};

export default useAxiosAPI;
