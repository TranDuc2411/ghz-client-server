import React from "react";

const A0201Lazy = React.lazy(() =>
  import("@components/A02.warehouse-management/A02.01.list/views/A0201Content")
);
const A0102Lazy = React.lazy(() =>
  import(
    "@components/A02.warehouse-management/A02.02.create/views/A0202Content"
  )
);
export const A02Router = [
  {
    name: "Danh sách sản phẩm trong kho",
    path: "/warehouse-management",
    layout: "function",
    element: <A0201Lazy />,
  },
  {
    name: "Tạo mới sản phẩm",
    path: "/warehouse-management/create",
    layout: "function",
    element: <A0102Lazy />,
  },
  {
    name: "Cập nhật sản phẩm",
    path: "/warehouse-management/update/:id",
    layout: "function",
    element: <A0102Lazy />,
  },
];
