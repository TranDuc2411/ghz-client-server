import React from "react";

const A0001Lazy = React.lazy(() =>
  import("@components/A00.common/A00.01.login/views/A0001Content")
);
export const A00Router = [
  {
    name: "Trang chủ",
    path: "/home",
    layout: "function",
    element: <></>,
  },
  {
    name: "Đăng nhập",
    path: "",
    layout: "login",
    element: <A0001Lazy />,
  },
  {
    name: "Đăng ký",
    path: "/Đăng ký",
    layout: "login",
    element: <A0001Lazy />,
  },
  {
    name: "Đổi mật khẩu",
    path: "/Đổi mật khẩu",
    layout: "login",
    element: <A0001Lazy />,
  },
];
