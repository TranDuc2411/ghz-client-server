import React from "react";

const A0501Lazy = React.lazy(() =>
  import("@components/A05.client-management/A05.01.list/views/A0501Content")
);

const A0502Lazy = React.lazy(() =>
  import("@components/A05.client-management/A05.02.create/views/A0502Content")
);
export const A05Router = [
  {
    name: "Danh sách khách hàng",
    path: "/client-management",
    layout: "function",
    element: <A0501Lazy />,
  },
  {
    name: "Tạo thông tin khách hàng",
    path: "/client-management/create",
    layout: "function",
    element: <A0502Lazy />,
  },
  {
    name: "Chỉnh sửa thông tin khách hàng",
    path: "/client-management/update/:id",
    layout: "function",
    element: <A0502Lazy />,
  },
];
