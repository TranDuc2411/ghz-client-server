import A0401Content from "@components/A04.post-mangement/A0401Content";

export const A04Router = [
  {
    name: "Quản lý cấu hình trạng thái",
    path: "/post-management",
    layout: "function",
    element: <A0401Content />,
  },
];
