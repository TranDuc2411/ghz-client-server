import React from "react";

const A0101Lazy = React.lazy(() =>
  import("@components/A01.admin-management/A01.01.list/views/A0101Content")
);
const A0102Lazy = React.lazy(() =>
  import("@components/A01.admin-management/A01.02.create/views/A0102Content")
);
const A0103Lazy = React.lazy(() =>
  import("@components/A01.admin-management/A01.03.user-info/views/A0103Content")
);
export const A01Router = [
  {
    name: "Danh sách tài khoản",
    path: "/admin-management",
    layout: "function",
    element: <A0101Lazy />,
  },
  {
    name: "Tạo mới Admin",
    path: "/admin-management/create",
    layout: "function",
    element: <A0102Lazy />,
  },
  {
    name: "Cập nhật Admin",
    path: "/admin-management/update/:id",
    layout: "function",
    element: <A0102Lazy />,
  },
  {
    name: "Cập nhật Admin",
    path: "/user-info",
    layout: "function",
    element: <A0103Lazy />,
  },
];
