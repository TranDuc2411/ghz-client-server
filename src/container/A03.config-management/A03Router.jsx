import A0303Content from "@components/A03.config-management/A03.03.product-name-management/views/A0303Content";
import React from "react";

const A0301Lazy = React.lazy(() =>
  import(
    "@components/A03.config-management/A03.01.status-management/views/A0301Content"
  )
);
const A0302Lazy = React.lazy(() =>
  import(
    "@components/A03.config-management/A03.02.stickers-management/views/A0302Content"
  )
);
const A0303Lazy = React.lazy(() =>
  import(
    "@components/A03.config-management/A03.03.product-name-management/views/A0303Content"
  )
);
const A0304Lazy = React.lazy(() =>
  import(
    "@components/A03.config-management/A03.03.product-category-management/views/A0304Content"
  )
);

export const A03Router = [
  {
    name: "Quản lý cấu hình trạng thái",
    path: "/config-status-management",
    layout: "function",
    element: <A0301Lazy />,
  },
  {
    name: "Quản lý cấu hình nhãn dán",
    path: "/config-stickers-management",
    layout: "function",
    element: <A0302Lazy />,
  },
  {
    name: "Quản lý cấu hình tên sản phẩm",
    path: "/product-name-management",
    layout: "function",
    element: <A0303Lazy />,
  },
  {
    name:"Quản lý danh mục sản phẩm",
    path: "/product-category-management",
    layout: "function",
    element: <A0304Lazy />,
  },
];
