
import React from "react";

const A0601Lazy = React.lazy(() =>
  import(
    "@components/A06.accesseoriess-management/A06.01.list/views/A0601Content"
  )
);
const A0602Lazy = React.lazy(() =>
  import(
    "@components/A06.accesseoriess-management/A06.02.create/views/A0602Content"
  )
);
export const A06Router = [
  {
    name: "Danh sách phụ kiện trong kho",
    path: "/accesseoriess-management",
    layout: "function",
    element: <A0601Lazy />,
  },
  {
    name: "Tạo mới phụ kiện",
    path: "/accesseoriess-management/create",
    layout: "function",
    element: <A0602Lazy />,
  },
  {
    name: "Cập nhật phụ kiện",
    path: "/accesseoriess-management/update/:id",
    layout: "function",
    element: <A0602Lazy />,
  },
];
