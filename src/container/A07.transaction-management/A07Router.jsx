
import React from "react";

const A0701Lazy = React.lazy(() =>
  import(
    "@components/A07.transaction-management/A07.01.list/views/A0701Content"
  )
);
const A0702Lazy = React.lazy(() =>
  import(
    "@components/A07.transaction-management/A07.02.create/views/A0702Content"
  )
);
export const A07Router = [
  {
    name: "Danh sách giao dịch",
    path: "/transaction-management",
    layout: "function",
    element: <A0701Lazy />,
  },
  {
    name: "Tạo mới giao dịch",
    path: "/transaction-management/create",
    layout: "function",
    element: <A0702Lazy />,
  },
  {
    name: "Cập nhật giao dịch",
    path: "/transaction-management/update/:id",
    layout: "function",
    element: <A0702Lazy />,
  },
];
