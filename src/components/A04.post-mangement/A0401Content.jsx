import ReactQuill from "react-quill";
// import "react-quill-antd/dist/index.css";

export default function A0401Content() {
  function onChange(e) {
    console.log(e);
  }

  const toolbarOptions = [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "code-block"],
    [
      // "link",
      // "image",
      // "video",
      // "formula"
    ],

    // [{ header: 1 }, { header: 2 }], // custom button values
    [{ list: "ordered" }, { list: "bullet" }, { list: "check" }],
    [{ script: "sub" }, { script: "super" }], // superscript/subscript
    [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
    [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],
    [{ align: [] }],

    ["clean"], // remove formatting button
  ];

  const modules = {
    toolbar: toolbarOptions,
  };
  return (
    <div>
      <h2>react-quill</h2>
      <div>
        <ReactQuill onChange={onChange} theme="snow" modules={modules}
        //  value={"<ol><li>Họ Tên: {name}</li><li>Ngày 01/01/2001</li></ol>"}
          />
      </div>
    </div>
  );
}
