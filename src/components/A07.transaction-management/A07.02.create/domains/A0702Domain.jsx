import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import A0702ContextService from "../services/A0702ContextService";

export default function A0702Domain() {
  const [context, contextService] = A0702ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const navigate = useNavigate();
  const mode = _.includes(location.pathname, "update") ? "update" : "create";
  const { id } = useParams();

  const initContext = {
    mode: mode,
    clientDetail: {},
    clientsOption: [],
    listMainProduct: [],
    productsOption: [],
    accessoriesOption: [],
    accessoriesList: [],
  };
  const contextRef = useRef(initContext);
  const initDomain = async () => {
    await contextService.initContext(initContext);

    if (!isSuperAdmin() && mode == "create") {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/transaction-management");
    } else {
      await getAllClient();
      await getAllProduct();
      await getAllAccessories();
    }
  };

  //-----------------------------------------
  async function getAllClient() {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/crm/client/getAll`,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          value: el.id,
          label: el.firstName + " " + el.lastName,
        }));
        contextRef.current.clientsOption = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function getAllProduct() {
    try {
      let response = await api.get(`/api/admin/warehouse/search`);
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: el.id,
          value: el.id,
          label: el.imei,
        }));
        contextRef.current.productsOption = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (err) {}
  }
  async function getAllAccessories() {
    try {
      let response = await api({
        method: "get",
        url: `/api/admin/warehouse/accessory/getall`,
        data: {
          isDeleted: true,
        },
      });
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: el._id,
          value: el._id,
          AccessoriesId: el._id,
          label: el.accessoryName,
        }));
        contextRef.current.accessoriesOption = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (err) {}
  }
  async function createTransaction(params) {
    try {
      common.backdrop(true);
      let request = {
        clientId: params?.clientId,
        listMainProduct: contextRef.current.listMainProduct,
        AccessoriesList: contextRef.current.accessoriesList,
        otherInformation: {
          createDate: "", //ngày in trên hoá đơn cái này để tính ngày hết hạn bảo hành
          note: "note somethings", // để người dùng tự nhập
        },
      };
      let response = await api({
        method: "post",
        url: `/api/admin/transaction/create`,
        data: request,
      });
      const { status } = response.data || {};
      if (status == 201) {
        message.success(response.data.message);
        // navigate("/transaction-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateAccesseoriess(params) {
    try {
      common.backdrop(true);
      let accesseoriessRequest = {
        accessoryName: params?.accessoryName,
        sellingPrice: params?.sellingPrice,
        purchasePrice: params?.purchasePrice,
        quantity: params?.quantity,
        supplier: params?.supplier,
      };
      let response = await api({
        method: "put",
        url: `/api/supper-admin/warehouses/accesseoriess/update/` + id,
        data: accesseoriessRequest,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/transaction-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function clientDetailOnchange(e) {
    try {
      let tmpClient = _.find(contextRef.current.clientsOption, (el) => {
        return el.id == e;
      });
      contextRef.current.clientDetail = tmpClient || [];
      contextService.updateContext(contextRef.current);
    } catch (error) {}
  }
  async function listMainProductOnchange(params) {
    try {
      let result = await contextRef.current.listMainProduct.findIndex((el) => {
        return el.id == params.id;
      });
      if (result == -1) {
        let tmpProduct = _.find(contextRef.current.productsOption, (el) => {
          return el.id == params.id;
        });
        tmpProduct = {
          ...tmpProduct,
          ...params,
        };
        contextRef.current.listMainProduct =
          [...contextRef.current.listMainProduct, tmpProduct] || [];
        contextService.updateContext(contextRef.current);
        message.success("Thêm mới sản phẩm thành công");
        return true;
      } else {
        message.error("Sản phẩm đã tồn tại");
        return false;
      }
    } catch (error) {}
  }
  async function handleDeleteListMainProduct(id) {
    try {
      const newArray = [];
      _.map(contextRef.current.listMainProduct, (el) => {
        if (el.id != id) {
          newArray.push(el);
        }
      });
      contextRef.current.listMainProduct = newArray;
      contextService.updateContext(contextRef.current);
    } catch (error) {}
  }
  async function accessoriesListOnchange(params) {
    try {
      let result = await contextRef.current.accessoriesList.findIndex((el) => {
        return el._id == params.id;
      });
      if (result == -1) {
        let tmp = _.find(contextRef.current.accessoriesOption, (el) => {
          return el._id == params.id;
        });
        tmp = {
          ...tmp,
          ...params,
        };
        contextRef.current.accessoriesList =
          [...contextRef.current.accessoriesList, tmp] || [];
        contextService.updateContext(contextRef.current);
        message.success("Thêm mới phụ kiện thành công");
        return true;
      } else {
        message.error("Phụ kiện đã tồn tại");
        return false;
      }
    } catch (error) {}
  }
  async function handleDeleteAccessoriesList(id) {
    try {
      const newArray = [];
      _.map(contextRef.current.accessoriesList, (el) => {
        if (el._id != id) {
          newArray.push(el);
        }
      });
      contextRef.current.accessoriesList = newArray;
      contextService.updateContext(contextRef.current);
    } catch (error) {}
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createTransaction,
    updateAccesseoriess,
    clientDetailOnchange,
    listMainProductOnchange,
    accessoriesListOnchange,
    handleDeleteListMainProduct,
    handleDeleteAccessoriesList,
  });

  return [context, domainInterface.current];
}
