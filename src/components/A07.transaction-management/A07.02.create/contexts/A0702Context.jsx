import { createSlice } from "@core/store/store";

const A0702Context = "A0702Context";
const A0702ActionList = Object.freeze({
  UpdateContext: A0702Context + "/update",
  ResetContext: A0702Context + "/reset",
});

const A0702InitalState = {};

const A0702Actions = {};

A0702Actions[A0702ActionList.UpdateContext] = (state, payload) => {
  if (A0702Context != payload?.slice) {
    return state;
  }
  return { ...state, A0702Context: payload.data };
};

A0702Actions[A0702ActionList.ResetContext] = (state, payload) => {
  if (A0702Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0702InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0702Context, A0702Actions, {
    ...A0702InitalState,
    ...data,
  });
};
export { A0702ActionList, A0702Actions, A0702Context, createContext };
