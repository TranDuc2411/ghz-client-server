import { Form, Input, Select, Space, Table, Typography } from "antd";
import TextArea from "antd/es/input/TextArea";
import moment from "moment";
import { getColumnsAccessories, getColumnsProduct } from "./constant";
export default function A0704Step4({ context, domain, mode, updateMode }) {
  const form = Form.useFormInstance();
  return (
    <>
      <Form
        className="w-full"
        labelCol={{
          span: 6,
        }}
        form={form}
        wrapperCol={{ flex: 1, span: 16 }}
        colon={false}
      >
        <Space direction="vertical" className="w-full mb-4">
          <>
            <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold mb-4">
              <Typography>Thông tin giao dịch</Typography>
            </div>
            <Form.Item label="Nhân viên">
              <Input
                disabled
                value={localStorage.getItem("fullName") || "Lý Mạnh Huy"}
              />
            </Form.Item>
            <Form.Item
              label="Khách hàng"
              name="clientId"
              className="mt-2-custom"
            >
              <Select disabled options={context?.clientsOption} />
            </Form.Item>
            <Form.Item label="Ngày tạo giao dịch">
              <Input disabled value={moment().format("DD/MM/YYYY")} />
            </Form.Item>
            <Form.Item label="Ghi chú" name="note" className="mt-2-custom">
              <TextArea placeholder="Nhập" />
            </Form.Item>
          </>
          {context?.listMainProduct.length != 0 && (
            <>
              <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold mt-4">
                <Typography>Bảng sản phẩm</Typography>
              </div>
              <Table
                className="w-full h-90"
                dataSource={context?.listMainProduct || []}
                bordered
                size="small"
                scroll={{
                  // x: "calc(700px + 50%)",
                  y: "auto",
                }}
                footer={null}
                columns={getColumnsProduct()}
              />
            </>
          )}
          {context.accessoriesList.length != 0 && (
            <>
              <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold mt-4">
                <Typography>Bảng phụ kiện</Typography>
              </div>
              <Table
                className="w-full h-90"
                dataSource={context?.accessoriesList || []}
                bordered
                size="small"
                scroll={{
                  // x: "calc(700px + 50%)",
                  y: "auto",
                }}
                footer={null}
                columns={getColumnsAccessories()}
              />
            </>
          )}
        </Space>
      </Form>
    </>
  );
}
