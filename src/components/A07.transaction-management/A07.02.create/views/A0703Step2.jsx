import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { InputNumberRequired, InputRequired } from "@common/formConfig";
import { configBgColor, formatCurrency } from "@common/utils";
import {
  Button,
  Form,
  InputNumber,
  Modal,
  Select,
  Space,
  Table,
  Tag,
  Typography,
} from "antd";
import _ from "lodash";
import { useState } from "react";

export default function A0703Step2({ context, domain, mode, updateMode }) {
  const [open, setOpen] = useState(false);
  function getColumns() {
    let result = [
      {
        title: "Stt",
        dataIndex: "index",
        key: "index",
        fixed: "left",
        width: 50,
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Tên máy",
        dataIndex: "productName",
        key: "productName",
        width: 100,
      },
      {
        title: "Imei",
        dataIndex: "imei",
        key: "imei",
        width: 100,
      },
      {
        title: "Giá bán sau khuyến mãi",
        dataIndex: "expense",
        key: "expense",
        width: 100,
        render: (value) => value && formatCurrency(value),
      },
      {
        title: "Bảo hành 1 đổi 1",
        dataIndex: "warrantyPolicy_1_1",
        key: "warrantyPolicy_1_1",
        width: 100,
      },
      {
        title: "Bảo hành dài hạn",
        dataIndex: "warrantyRepairs",
        key: "warrantyRepairs",
        width: 100,
      },
      {
        title: "Thông tin máy",
        dataIndex: "configuration",
        width: 120,
      },
      {
        title: "Hình thức",
        dataIndex: "status",
        key: "status",
        width: 130,
        render: (value, record) => {
          return _.map(value, (el, index) => (
            <Tag key={record?.key + index} color={el?.color}>
              <div className={`text-wrap`}>{el?.title}</div>
            </Tag>
          ));
        },
      },
      {
        key: "action",
        width: 50,
        fixed: "right",
        render: (record) => (
          <Tag
            color="red"
            onClick={() => domain.handleDeleteListMainProduct(record.id)}
          >
            <DeleteOutlined className="text-xl text-red-6" />
          </Tag>
        ),
      },
    ];

    return result || [];
  }
  function ModalCreate() {
    const [formCreate] = Form.useForm();
    async function onFinsh() {
      await formCreate.validateFields();
      let params = await formCreate.getFieldsValue(true);
      let result = await domain.listMainProductOnchange(params);
      if (result) {
        setOpen(false);
      }
    }
    return (
      <Modal
        title="Thêm mới sản phẩm"
        open={open}
        width={600}
        onCancel={() => setOpen(false)}
        closable={false}
        footer={[
          <Button key={1} onClick={() => setOpen(false)}>
            Hủy
          </Button>,
          <Button
            key={2}
            className="bg-blue-6 text-white ml-2"
            onClick={onFinsh}
          >
            Xác nhận
          </Button>,
        ]}
      >
        <Form
          className="w-full"
          form={formCreate}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Mã sản phẩm"
              name={"id"}
              rules={[...InputNumberRequired]}
            >
              <Select
                showSearch
                placeholder="Chọn"
                disabled={mode != "create" && updateMode}
                options={context?.productsOption}
                filterOption={(input, option) =>
                  (option?.label ?? "")
                    .toLowerCase()
                    .includes(input.toLowerCase())
                }
              />
            </Form.Item>
            <Form.Item
              label="Giá bán sau khuyến mãi"
              name="expense"
              rules={[...InputNumberRequired]}
            >
              <InputNumber
                placeholder="Nhập"
                className="w-full"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
              />
            </Form.Item>
            <Form.Item
              label="Bảo hành 1 đổi 1"
              name="warrantyPolicy_1_1"
              rules={[...InputRequired]}
            >
              <Select
                placeholder="Chọn"
                options={[
                  {
                    value: "7 ngày",
                    label: "7 ngày",
                  },
                  {
                    value: "15 ngày",
                    label: "15 ngày",
                  },
                  {
                    value: "30 ngày",
                    label: "30 ngày",
                  },
                  {
                    value: "Không bảo hành",
                    label: "Không bảo hành",
                  },
                ]}
              />
            </Form.Item>
            <Form.Item
              label="Bảo hành dài hạn"
              name="warrantyRepairs"
              rules={[...InputRequired]}
            >
              <Select
                placeholder="Chọn"
                options={[
                  {
                    value: "3 tháng",
                    label: "3 tháng",
                  },
                  {
                    value: "6 tháng",
                    label: "6 tháng",
                  },
                  {
                    value: "Theo hãng",
                    label: "Theo hãng",
                  },
                  {
                    value: "Không bảo hành",
                    label: "Không bảo hành",
                  },
                ]}
              />
            </Form.Item>
          </Space>
        </Form>
      </Modal>
    );
  }
  return (
    <Space direction="vertical" className="w-full">
      <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold">
        <Typography>Bảng sản phẩm</Typography>
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={() => setOpen(true)}
        >
          Thêm mới
        </Button>
      </div>
      <ModalCreate />
      <Table
        className="w-full h-90"
        dataSource={context?.listMainProduct || []}
        bordered
        size="small"
        rowClassName={(record) => `${configBgColor(record?.state?.color)}`}
        scroll={{
          // x: "calc(700px + 50%)",
          y: "auto",
        }}
        footer={null}
        columns={getColumns()}
      />
    </Space>
  );
}
