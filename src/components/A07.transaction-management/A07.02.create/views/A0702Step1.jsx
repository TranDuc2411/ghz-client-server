import { InputMax, InputRequired } from "@common/formConfig";
import { Col, Form, Input, Row, Select, Space, Typography } from "antd";
import moment from "moment";

export default function A0702Step1({ context, domain, mode, updateMode }) {
  const { clientDetail } = context || {};
  return (
    <>
      
      <>
        <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold mb-4">
          <Typography>Thông tin khách hàng</Typography>
        </div>
        <Space direction="vertical" className="w-full">
          <Form.Item
            label="Khách hàng"
            name="clientId"
            rules={[...InputRequired, ...InputMax]}
            className="mt-2-custom"
          >
            <Select
              showSearch
              placeholder="Chọn"
              disabled={mode != "create" && updateMode}
              options={context?.clientsOption}
              filterOption={(input, option) =>
                (option?.label ?? "")
                  .toLowerCase()
                  .includes(input.toLowerCase())
              }
              onChange={(e) => domain.clientDetailOnchange(e)}
            />
          </Form.Item>

          <Form.Item label="Họ" className="mt-2-custom">
            <Input disabled={true} value={clientDetail?.firstName} />
          </Form.Item>
          <Form.Item label="Tên" className="mt-2-custom">
            <Input disabled={true} value={clientDetail?.lastName} />
          </Form.Item>
          <Form.Item label="Số điện thoại">
            {!!clientDetail?.phoneNumber ? (
              clientDetail?.phoneNumber?.map((field, index) => (
                <Row className="mb-2">
                  <Col span={20}>
                    <Form.Item>
                      <Input disabled={true} value={field} />
                    </Form.Item>
                  </Col>
                </Row>
              ))
            ) : (
              <Form.Item>
                <Input disabled={true} />
              </Form.Item>
            )}
          </Form.Item>
          <Form.Item label="Địa chỉ">
            {!!clientDetail?.address ? (
              clientDetail?.address?.map((field, index) => (
                <Row className="mb-2">
                  <Col span={20}>
                    <Form.Item>
                      <Input.TextArea disabled={true} value={field} />
                    </Form.Item>
                  </Col>
                </Row>
              ))
            ) : (
              <Form.Item>
                <Input.TextArea disabled={true} />
              </Form.Item>
            )}
          </Form.Item>
        </Space>
      </>
    </>
  );
}
