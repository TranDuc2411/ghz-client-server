import { Button, Card, Form, Steps } from "antd";
import { useEffect, useState } from "react";
import A0702Domain from "../domains/A0702Domain";
import A0702Step1 from "./A0702Step1";
import A0703Step2 from "./A0703Step2";
import A0703Step3 from "./A0703Step3";
import A0704Step4 from "./A0704Step4";

export default function A0702Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0702Domain();
  const { mode } = context || {};
  const [updateMode, setUpdateMode] = useState(true);
  const [step, setStep] = useState(0);
  const next = async () => {
    await form.validateFields();
    setStep(step + 1);
  };
  const prev = () => {
    setStep(step - 1);
  };

  useEffect(() => {
    domain.initDomain();
  }, []);

  async function createTransaction() {
    let params = await form.getFieldsValue(true);
    await domain.createTransaction(params);
  }

  async function updateAccesseoriess() {
    await form.validateFields();
    let values = await form.getFieldsValue(true);
    await domain.updateAccesseoriess(values);
  }

  return (
    <div className="h-auto w-auto">
      <Steps
        className="mb-2 px-20"
        current={step}
        items={[
          {
            title: "Thông tin giao dich",
          },
          {
            title: "Thông tin sản phẩm",
          },
          {
            title: "Thông tin phụ kiện",
          },
          {
            title: "Xác nhận tạo giao dịch",
          },
        ]}
      />
      <Card
        title={mode == "update" ? "Chỉnh sửa giao dịch" : "Tạo mới giao dịch"}
        className="w-full"
      >
        <>
          <Form
            className="w-full"
            form={form}
            labelCol={{
              span: 6,
            }}
            wrapperCol={{ flex: 1, span: 16 }}
            colon={false}
          >
            {step == 0 && (
              <A0702Step1
                context={context}
                domain={domain}
                mode={mode}
                updateMode={updateMode}
              />
            )}
            {step == 1 && (
              <A0703Step2
                context={context}
                domain={domain}
                mode={mode}
                updateMode={updateMode}
              />
            )}
            {step == 2 && (
              <A0703Step3
                context={context}
                domain={domain}
                mode={mode}
                updateMode={updateMode}
              />
            )}
            {step == 3 && (
              <A0704Step4
                context={context}
                domain={domain}
                mode={mode}
                updateMode={updateMode}
              />
            )}
            <div className="mt-2">
              <Form.Item label=" " className="">
                {step != 0 && (
                  <Button
                    style={{
                      margin: "0 8px",
                    }}
                    onClick={() => prev()}
                  >
                    Quay lại
                  </Button>
                )}
                {step == 3 && (
                  <Button type="primary" onClick={() => createTransaction()}>
                    Xác nhận tạo mới
                  </Button>
                )}
                {step != 3 && (
                  <Button type="primary" onClick={() => next()}>
                    Tiếp tục
                  </Button>
                )}
              </Form.Item>
            </div>
          </Form>
        </>
      </Card>
    </div>
  );
}
