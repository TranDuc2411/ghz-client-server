import { PlusOutlined, DeleteOutlined } from "@ant-design/icons";
import { InputNumberRequired } from "@common/formConfig";
import { configBgColor, formatCurrency } from "@common/utils";
import {
  Button,
  Form,
  InputNumber,
  Modal,
  Select,
  Space,
  Table,
  Tag,
  Typography,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { useState } from "react";

export default function A0703Step3({ context, domain, mode, updateMode }) {
  const [open, setOpen] = useState(false);
  function getColumns() {
    let result = [
      {
        title: "Stt",
        dataIndex: "index",
        key: "index",
        fixed: "left",
        width: 50,
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Tên phụ kiện",
        dataIndex: "accessoryName",
        key: "accessoryName",
        width: 100,
      },
      {
        title: "Giá bán sau khuyến mãi",
        dataIndex: "expense",
        key: "expense",
        width: 100,
        render: (value) => value && formatCurrency(value),
      },
      {
        title: "Số lượng",
        dataIndex: "note",
        key: "note",
        width: 100,
      },
      {
        title: "Ghi chú",
        dataIndex: "count",
        key: "count",
        width: 100,
        render: (value) => value && formatCurrency(value),
      },
      {
        title: "Nhà cung cấp",
        dataIndex: "supplier",
        key: "supplier",
        width: 100,
      },
      {
        key: "action",
        width: 50,
        fixed: "right",
        render: (record) => (
          <Tag
            color="red"
            onClick={() => domain.handleDeleteAccessoriesList(record._id)}
          >
            <DeleteOutlined className="text-xl text-red-6" />
          </Tag>
        ),
      },
    ];

    return result || [];
  }

  function ModalCreate() {
    const [formCreate] = Form.useForm();
    async function onFinsh() {
      await formCreate.validateFields();
      let params = await formCreate.getFieldsValue(true);
      let result = await domain.accessoriesListOnchange(params);
      if (result) {
        setOpen(false);
      }
    }
    return (
      <Modal
        title="Thêm mới phụ kiện"
        open={open}
        width={600}
        onCancel={() => setOpen(false)}
        closable={false}
        footer={[
          <Button key={1} onClick={() => setOpen(false)}>
            Hủy
          </Button>,
          <Button
            key={2}
            className="bg-blue-6 text-white ml-2"
            onClick={() => onFinsh()}
          >
            Xác nhận
          </Button>,
        ]}
      >
        <Form
          className="w-full"
          form={formCreate}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên phụ kiện"
              rules={[...InputNumberRequired]}
              name="id"
            >
              <Select
                showSearch
                placeholder="Chọn"
                disabled={mode != "create" && updateMode}
                options={context?.accessoriesOption}
                filterOption={(input, option) =>
                  (option?.label ?? "")
                    .toLowerCase()
                    .includes(input.toLowerCase())
                }
              />
            </Form.Item>
            <Form.Item
              label="Giá bán sau khuyến mãi"
              name="expense"
              rules={[...InputNumberRequired]}
            >
              <InputNumber
                placeholder="Nhập"
                className="w-full"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
              />
            </Form.Item>
            <Form.Item
              label="Số lượng"
              name="count"
              rules={[...InputNumberRequired]}
            >
              <InputNumber
                placeholder="Nhập"
                className="w-full"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
              />
            </Form.Item>
            <Form.Item label="Ghi chú" name="note">
              <TextArea placeholder="Nhập" className="w-full" />
            </Form.Item>
          </Space>
        </Form>
      </Modal>
    );
  }
  return (
    <Space direction="vertical" className="w-full">
      <div className="w-full flex justify-between h-16 px-6 flex items-center bg-grey-3 text-grey-9 font-bold">
        <Typography>Bảng phụ kiện</Typography>
        <Button
          type="primary"
          icon={<PlusOutlined />}
          onClick={() => setOpen(true)}
        >
          Thêm mới
        </Button>
      </div>
      <ModalCreate />
      <Table
        className="w-full h-90"
        dataSource={context?.accessoriesList || []}
        bordered
        size="small"
        rowClassName={(record) => `${configBgColor(record?.state?.color)}`}
        scroll={{
          // x: "calc(700px + 50%)",
          y: "auto",
        }}
        footer={null}
        columns={getColumns()}
      />
    </Space>
  );
}
