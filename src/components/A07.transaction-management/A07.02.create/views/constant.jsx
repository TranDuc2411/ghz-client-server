import { formatCurrency } from "@common/utils";
import { Tag } from "antd";
import _ from "lodash";

export function getColumnsProduct() {
  let result = [
    {
      title: "Stt",
      dataIndex: "index",
      key: "index",
      fixed: "left",
      width: 50,
      render: (_value, _record, index) => index + 1,
    },
    {
      title: "Tên máy",
      dataIndex: "productName",
      key: "productName",
      width: 100,
    },
    {
      title: "Imei",
      dataIndex: "imei",
      key: "imei",
      width: 100,
    },
    {
      title: "Giá bán sau khuyến mãi",
      dataIndex: "expense",
      key: "expense",
      width: 100,
      render: (value) => value && formatCurrency(value),
    },
    {
      title: "Bảo hành 1 đổi 1",
      dataIndex: "warrantyPolicy_1_1",
      key: "warrantyPolicy_1_1",
      width: 100,
    },
    {
      title: "Bảo hành dài hạn",
      dataIndex: "warrantyRepairs",
      key: "warrantyRepairs",
      width: 100,
    },
    {
      title: "Thông tin máy",
      dataIndex: "configuration",
      width: 120,
    },
    {
      title: "Hình thức",
      dataIndex: "status",
      key: "status",
      width: 130,
      render: (value, record) => {
        return _.map(value, (el, index) => (
          <Tag key={record?.key + index} color={el?.color}>
            <div className={`text-wrap`}>{el?.title}</div>
          </Tag>
        ));
      },
    },
  ];

  return result || [];
}
export function getColumnsAccessories() {
  let result = [
    {
      title: "Stt",
      dataIndex: "index",
      key: "index",
      fixed: "left",
      width: 50,
      render: (_value, _record, index) => index + 1,
    },
    {
      title: "Tên phụ kiện",
      dataIndex: "accessoryName",
      key: "accessoryName",
      width: 100,
    },
    {
      title: "Giá bán sau khuyến mãi",
      dataIndex: "expense",
      key: "expense",
      width: 100,
      render: (value) => value && formatCurrency(value),
    },
    {
      title: "Số lượng",
      dataIndex: "note",
      key: "note",
      width: 100,
    },
    {
      title: "Ghi chú",
      dataIndex: "count",
      key: "count",
      width: 100,
      render: (value) => value && formatCurrency(value),
    },
    {
      title: "Nhà cung cấp",
      dataIndex: "supplier",
      key: "supplier",
      width: 100,
    },
  ];

  return result || [];
}
