import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0702ActionList,
  A0702Context,
  createContext,
} from '../contexts/A0702Context';

export default function A0702ContextService() {
  const context = useStore()[A0702Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0702Context,
      type: A0702ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0702Context,
      type: A0702ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
