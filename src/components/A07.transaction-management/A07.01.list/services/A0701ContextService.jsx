import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0701ActionList,
  A0701Context,
  createContext,
} from '../contexts/A0701Context';

export default function A0701ContextService() {
  const context = useStore()[A0701Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0701Context,
      type: A0701ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0701Context,
      type: A0701ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
