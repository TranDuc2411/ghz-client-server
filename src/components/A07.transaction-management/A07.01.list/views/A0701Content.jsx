import {
  EditOutlined,
  FileSearchOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { configBgColor, getScreenWidth, isSuperAdmin } from "@common/utils";
import { Button, Card, Input, Table, Typography } from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import A0701Domain from "../domains/A0701Domain";
import A0701Filter from "./A0701Filter";

export default function A0701Content() {
  const [context, domain] = A0701Domain();
  const [filter, setFilter] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    setDataSource(context?.dataTable);
  }, [context?.dataTable]);

  function getColumns() {
    let result = [
      {
        title: "Stt",
        dataIndex: "index",
        key: "index",
        fixed: "left",
        width: 50,
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Mã giao dịch",
        dataIndex: "accessoryName",
        key: "accessoryName",
        width: 100,
      },
      {
        title: "Khách hàng",
        dataIndex: "supplier",
        key: "supplier",
        width: 100,
      },
      {
        title: "Nhân viên",
        dataIndex: "supplier",
        key: "supplier",
        width: 100,
      },
      {
        title: "Tổng tiền",
        dataIndex: "supplier",
        key: "supplier",
        width: 100,
      },
      {
        key: "action",
        width: 50,
        fixed: "right",
        render: (record) => (
          <Typography.Link
            onClick={() =>
              navigate(`/transaction-management/update/${record?._id}`)
            }
          >
            <EditOutlined className="text-xl" />
          </Typography.Link>
        ),
      },
    ];

    return result || [];
  }
  return (
    <div className="h-auto w-auto">
      <A0701Filter filter={filter} setFilter={setFilter} />

      <Card
        className="w-auto"
        title={<span className={`break-words`}>Danh sách giao dịch</span>}
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Input
              placeholder="Tìm kiếm"
              className="ml-2 w-[150px]"
              onChange={async (e) => {
                const value = e.target.value.toLowerCase().trim();
                let result = _.filter(context.dataTable, (el) =>
                  el?.accessoryName?.toLowerCase().trim().includes(value)
                );
                setDataSource(result);
              }}
            />
            <Button
              className="bg-grey-3 ml-2"
              onClick={() => setFilter(true)}
              icon={<FileSearchOutlined />}
            >
              {getScreenWidth() == "pc" && "Bộ lọc"}
            </Button>
            {isSuperAdmin() && (
              <Button
                className="bg-blue-6 text-white ml-2"
                onClick={() => navigate("/transaction-management/create")}
                icon={<PlusOutlined />}
              >
                {getScreenWidth() == "pc" && "Tạo mới"}
              </Button>
            )}
          </div>
        }
      >
        <Table
          className="w-full h-90"
          dataSource={dataSource}
          bordered
          size="small"
          rowClassName={(record) => `${configBgColor(record?.state?.color)}`}
          scroll={{
            // x: "calc(700px + 50%)",
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}
