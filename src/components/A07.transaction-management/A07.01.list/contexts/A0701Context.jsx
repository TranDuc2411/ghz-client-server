import { createSlice } from "@core/store/store";

const A0701Context = "A0701Context";
const A0701ActionList = Object.freeze({
  UpdateContext: A0701Context + "/update",
  ResetContext: A0701Context + "/reset",
});

const A0701InitalState = {};

const A0701Actions = {};

A0701Actions[A0701ActionList.UpdateContext] = (state, payload) => {
  if (A0701Context != payload?.slice) {
    return state;
  }
  return { ...state, A0701Context: payload.data };
};

A0701Actions[A0701ActionList.ResetContext] = (state, payload) => {
  if (A0701Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0701InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0701Context, A0701Actions, {
    ...A0701InitalState,
    ...data,
  });
};
export { A0701ActionList, A0701Actions, A0701Context, createContext };
