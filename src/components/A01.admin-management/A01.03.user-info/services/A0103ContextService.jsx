import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0103ActionList,
  A0103Context,
  createContext,
} from '../contexts/A0103Context';

export default function A0103ContextService() {
  const context = useStore()[A0103Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0103Context,
      type: A0103ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0103Context,
      type: A0103ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
