import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import { useRef } from "react";
import A0103ContextService from "../services/A0103ContextService";

export default function A0103Domain() {
  const [context, contextService] = A0103ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const contextRef = useRef({
    accountDetail: {},
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    await getUserInfo();
  };

  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  async function getUserInfo() {
    try {
      common.backdrop(true);
      let response = await api.get(`/api/admin/get-info`);
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        contextRef.current.accountDetail = data || {};
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  const domainInterface = useRef({
    initDomain,
  });

  return [context, domainInterface.current];
}
