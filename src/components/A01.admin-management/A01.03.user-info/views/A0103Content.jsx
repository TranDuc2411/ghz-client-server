import { InputMax, InputRequired, InputTextAreaMax } from "@common/formConfig";
import { Card, Form, Input, Radio, Space } from "antd";
import { useEffect, useState } from "react";
import A0103Domain from "../domains/A0103Domain";

export default function A0103Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0103Domain();
  const { accountDetail } = context || {};

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    form.setFieldsValue({
      email: accountDetail?.email,
      firstName: accountDetail?.firstName,
      lastName: accountDetail?.lastName,
      phoneNumber: accountDetail?.phoneNumber,
      address: accountDetail?.address,
      role: accountDetail?.role,
    });
  }, [accountDetail]);

  return (
    <div className="h-auto w-auto">
      <Card title={"Thông tin tài khoản"} className="w-full">
        <Form
          className="w-full"
          form={form}
          // labelAlign="right"
          // labelCol={{ flex:"300px" }}
          labelCol={{
            span: 8,
          }}
          initialValues={{
            role: 0,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
          // onFinish={createAccount}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Email"
              name="email"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" disabled />
            </Form.Item>
            <Form.Item
              label="Họ"
              name="firstName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" disabled />
            </Form.Item>
            <Form.Item
              label="Tên"
              name="lastName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" disabled />
            </Form.Item>
            <Form.Item
              label="Số điện thoại"
              name="phoneNumber"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" disabled />
            </Form.Item>
            <Form.Item label="Vai trò" name="role" className="mt-2-custom">
              <Radio.Group disabled>
                <Radio value={0}>Admin</Radio>
                <Radio value={1}>Super Admin</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label="Địa chỉ"
              name="address"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea placeholder="Nhập thông tin" disabled />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </div>
  );
}
