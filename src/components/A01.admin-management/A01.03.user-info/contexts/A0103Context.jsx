import { createSlice } from "@core/store/store";

const A0103Context = "A0103Context";
const A0103ActionList = Object.freeze({
  UpdateContext: A0103Context + "/update",
  ResetContext: A0103Context + "/reset",
});

const A0103InitalState = {};

const A0103Actions = {};

A0103Actions[A0103ActionList.UpdateContext] = (state, payload) => {
  if (A0103Context != payload?.slice) {
    return state;
  }
  return { ...state, A0103Context: payload.data };
};

A0103Actions[A0103ActionList.ResetContext] = (state, payload) => {
  if (A0103Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0103InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0103Context, A0103Actions, {
    ...A0103InitalState,
    ...data,
  });
};
export { A0103ActionList, A0103Actions, A0103Context, createContext };
