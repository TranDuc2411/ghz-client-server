import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import A0102ContextService from "../services/A0102ContextService";

export default function A0102Domain() {
  const [context, contextService] = A0102ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();
  const mode = _.includes(location.pathname, "update") ? "update" : "create";
  const { id } = useParams();
  const contextRef = useRef({
    mode: mode,
    accountDetail: {},
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      if (mode == "update") {
        await getAccount(id);
      }
    }
  };

  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  async function getAccount(id) {
    try {
      common.backdrop(true);
      let response = await api.get(`/api/supper-admin/account/detail/` + id);
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        contextRef.current.accountDetail = data || {};
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function createAccount(params) {
    try {
      common.backdrop(true);
      let response = await api.post(`/api/supper-admin/account/create`, {
        email: params?.email,
        password: params?.password,
        firstName: params?.firstName,
        lastName: params?.lastName,
        phoneNumber: params?.phoneNumber,
        address: params?.address,
        role: params?.role,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/admin-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateAccount(params) {
    try {
      common.backdrop(true);
      let response = await api.put(`/api/supper-admin/account/update/` + id, {
        email: params?.email,
        password: params?.password,
        firstName: params?.firstName,
        lastName: params?.lastName,
        phoneNumber: params?.phoneNumber,
        address: params?.address,
        role: params?.role,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/admin-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  const domainInterface = useRef({
    initDomain,
    createAccount,
    updateAccount,
  });

  return [context, domainInterface.current];
}
