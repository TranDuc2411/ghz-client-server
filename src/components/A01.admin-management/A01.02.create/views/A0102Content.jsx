import { EditOutlined } from "@ant-design/icons";
import { InputMax, InputRequired, InputTextAreaMax } from "@common/formConfig";
import { Button, Card, Form, Input, Radio, Space } from "antd";
import { useEffect, useState } from "react";
import { FaCheckCircle } from "react-icons/fa";
import { IoCaretBackOutline } from "react-icons/io5";
import A0102Domain from "../domains/A0102Domain";

export default function A0102Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0102Domain();
  const { accountDetail, mode } = context || {};
  const [updateMode, setUpdateMode] = useState(true);

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    console.log("accountDetail", accountDetail);
    if (mode == "update") {
      form.setFieldsValue({
        email: accountDetail?.email,
        firstName: accountDetail?.firstName,
        lastName: accountDetail?.lastName,
        phoneNumber: accountDetail?.phoneNumber,
        address: accountDetail?.address,
        role: accountDetail?.role,
      });
    } else {
      form.resetFields();
    }
  }, [accountDetail]);

  async function createAccount(values) {
    await domain.createAccount(values);
  }

  async function updateAccount() {
    await form.validateFields();
    let values = await form.getFieldsValue(true);
    await domain.updateAccount(values);
  }

  return (
    <div className="h-auto w-auto">
      <Card
        title={mode == "update" ? "Chỉnh sửa tài khoản" : "Tạo mới tài khoản"}
        className="w-full"
      >
        <Form
          className="w-full"
          form={form}
          // labelAlign="right"
          // labelCol={{ flex:"300px" }}
          labelCol={{
            span: 8,
          }}
          initialValues={{
            role: 0,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
          onFinish={createAccount}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Email"
              name="email"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" disabled={mode != "create"} />
            </Form.Item>
            {mode == "create" && (
              <Form.Item
                label="Mật khẩu"
                name="password"
                rules={[...InputRequired, ...InputMax]}
                className="mt-2-custom"
              >
                <Input.Password
                  placeholder="Nhập thông tin"
                  disabled={mode != "create" && updateMode}
                />
              </Form.Item>
            )}
            <Form.Item
              label="Họ"
              name="firstName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Tên"
              name="lastName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Số điện thoại"
              name="phoneNumber"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item label="Vai trò" name="role" className="mt-2-custom">
              <Radio.Group disabled={mode != "create"}>
                <Radio value={0}>Admin</Radio>
                <Radio value={1}>Super Admin</Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              label="Địa chỉ"
              name="address"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>

            {mode == "create" && (
              <Form.Item label=" ">
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-blue-6 text-white"
                  icon={<FaCheckCircle />}
                >
                  Xác nhận tạo mới
                </Button>
              </Form.Item>
            )}
            {mode == "update" && (
              <>
                {updateMode ? (
                  <Form.Item label=" ">
                    <Button
                      type="primary"
                      // htmlType="submit"
                      className="bg-blue-6 text-white"
                      onClick={() => setUpdateMode(false)}
                      icon={<EditOutlined className="text-xl text-white" />}
                    >
                      Chỉnh sửa
                    </Button>
                  </Form.Item>
                ) : (
                  <Form.Item label=" " className="">
                    <Button
                      type="primary"
                      className="bg-grey-3 ml-2 text-black mr-4"
                      onClick={() => setUpdateMode(true)}
                      icon={<IoCaretBackOutline />}
                    >
                      Quay lại
                    </Button>
                    <Button
                      type="primary"
                      className="bg-blue-6 text-white"
                      onClick={() => updateAccount()}
                      icon={<FaCheckCircle />}
                    >
                      Xác nhận chỉnh sửa
                    </Button>
                  </Form.Item>
                )}
              </>
            )}
          </Space>
        </Form>
      </Card>
    </div>
  );
}
