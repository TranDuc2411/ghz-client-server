import { createSlice } from "@core/store/store";

const A0102Context = "A0102Context";
const A0102ActionList = Object.freeze({
  UpdateContext: A0102Context + "/update",
  ResetContext: A0102Context + "/reset",
});

const A0102InitalState = {};

const A0102Actions = {};

A0102Actions[A0102ActionList.UpdateContext] = (state, payload) => {
  if (A0102Context != payload?.slice) {
    return state;
  }
  return { ...state, A0102Context: payload.data };
};

A0102Actions[A0102ActionList.ResetContext] = (state, payload) => {
  if (A0102Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0102InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0102Context, A0102Actions, {
    ...A0102InitalState,
    ...data,
  });
};
export { A0102ActionList, A0102Actions, A0102Context, createContext };
