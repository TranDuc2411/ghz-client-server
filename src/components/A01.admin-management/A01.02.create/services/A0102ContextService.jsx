import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0102ActionList,
  A0102Context,
  createContext,
} from '../contexts/A0102Context';

export default function A0102ContextService() {
  const context = useStore()[A0102Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0102Context,
      type: A0102ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0102Context,
      type: A0102ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
