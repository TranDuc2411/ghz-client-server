import { createSlice } from "@core/store/store";

const A0101Context = "A0101Context";
const A0101ActionList = Object.freeze({
  UpdateContext: A0101Context + "/update",
  ResetContext: A0101Context + "/reset",
});

const A0101InitalState = {};

const A0101Actions = {};

A0101Actions[A0101ActionList.UpdateContext] = (state, payload) => {
  if (A0101Context != payload?.slice) {
    return state;
  }
  return { ...state, A0101Context: payload.data };
};

A0101Actions[A0101ActionList.ResetContext] = (state, payload) => {
  if (A0101Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0101InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0101Context, A0101Actions, {
    ...A0101InitalState,
    ...data,
  });
};
export { A0101ActionList, A0101Actions, A0101Context, createContext };
