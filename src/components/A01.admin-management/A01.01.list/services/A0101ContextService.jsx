import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0101ActionList,
  A0101Context,
  createContext,
} from '../contexts/A0101Context';

export default function A0101ContextService() {
  const context = useStore()[A0101Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0101Context,
      type: A0101ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0101Context,
      type: A0101ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
