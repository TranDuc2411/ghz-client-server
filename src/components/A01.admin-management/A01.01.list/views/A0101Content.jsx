import {
  EditOutlined,
  FileSearchOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { getScreenWidth } from "@common/utils";
import { Button, Card, Input, Table, Typography } from "antd";
import _ from "lodash";
import moment from "moment";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import A0101Domain from "../domains/A0101Domain";
import A0101Filter from "./A0101Filter";

export default function A0101Content() {
  const [context, domain] = A0101Domain();
  const [filter, setFilter] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const navigate = useNavigate();
  
  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    setDataSource(context?.dataTable);
  }, [context?.dataTable]);

  function getColumns() {
    let result = [
      {
        title: "STT",
        dataIndex: "index",
        width: '7%',
        render: (value, record, index) => index + 1,
      },
      {
        title: "Email",
        dataIndex: "email",
        width: '20%',
      },
      {
        title: "Họ tên",
        dataIndex: "full_name",
        width: '20%',
        render: (_value, record, index) =>
          `${record.firstName} ${record.lastName}`,
      },
      {
        title: "Vai trò",
        dataIndex: "role",
        width: '15%',
        render: (value) => (value == 1 ? "super-admin" : "admin"),
      },
      {
        title: "Số điện thoại",
        dataIndex: "phoneNumber",
        width: '15%',
      },
      {
        title: "Ngày tạo",
        dataIndex: "createdAt",
        width: '15%',
        render: (value) => value && moment(value).format("DD/MM/YYYY"),
      },
      {
        title: "Tác vụ",
        width: '7%',
        render: (record) => (
          <Typography.Link
            onClick={() => navigate(`/admin-management/update/${record?.id}`)}
          >
            <EditOutlined className="text-xl" />
          </Typography.Link>
        ),
      },
    ];
    return result || [];
  }

  return (
    <div className="h-auto w-auto">
      <A0101Filter filter={filter} setFilter={setFilter} />

      <Card
        className="w-auto"
        title={
          <span
            className={`break-words`}
          >
            Danh sách tài khoản
          </span>
        }
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Input
              placeholder="Tìm kiếm"
              className="ml-2 w-[150px]"
              onChange={async (e) => {
                const value = e.target.value.toLowerCase().trim();
                let result = _.filter(
                  context.dataTable,
                  (el) =>
                    el?.email?.toLowerCase().trim().includes(value) ||
                    el?.firstName?.toLowerCase().trim().includes(value) ||
                    el?.lastName?.toLowerCase().trim().includes(value)
                );
                setDataSource(result);
              }}
            />
            <Button
              className="bg-grey-3 ml-2"
              onClick={() => setFilter(true)}
              icon={<FileSearchOutlined />}
            >
              {getScreenWidth() == "pc" && "Bộ lọc"}
            </Button>
            <Button
              className="bg-blue-6 text-white ml-2"
              onClick={() => navigate("/admin-management/create")}
              icon={<PlusOutlined />}
            >
              {getScreenWidth() == "pc" && "Tạo mới"}
            </Button>
          </div>
        }
      >
        <Table
          className="w-full"
          dataSource={dataSource}
          ordered
          size="small"
          scroll={{
            // x: "calc(700px + 50%)",
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}
