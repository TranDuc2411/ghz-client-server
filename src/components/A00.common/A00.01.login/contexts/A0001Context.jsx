import { createSlice } from "@core/store/store";

const A0001Context = "A0001Context";
const A0001ActionList = Object.freeze({
  UpdateContext: A0001Context + "/update",
  ResetContext: A0001Context + "/reset",
});

const A0001InitalState = {};

const A0001Actions = {};

A0001Actions[A0001ActionList.UpdateContext] = (state, payload) => {
  if (A0001Context != payload?.slice) {
    return state;
  }
  return { ...state, A0001Context: payload.data };
};

A0001Actions[A0001ActionList.ResetContext] = (state, payload) => {
  if (A0001Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0001InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0001Context, A0001Actions, {
    ...A0001InitalState,
    ...data,
  });
};
export { A0001ActionList, A0001Actions, A0001Context, createContext };
