import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import A0001ContextService from "../services/A0001ContextService";

export default function A0001Domain() {
  const common = UseCommon();
  const navigate = useNavigate();
  const [context, contextService] = A0001ContextService();
  const contextRef = useRef({});
  const api = useAxiosAPI();
  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
  };
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  async function login({ email, password }) {
    try {
      common.backdrop(true);
      let response = await api.post(`/api/admin/login`, {
        email: email,
        password: password,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        message.success(response?.data?.message);
        localStorage.setItem("accessToken", data?.accessToken);
        localStorage.setItem("refreshToken", data?.refreshToken);
        await getUserInfo();
      } else {
        message.error(response?.data?.message);
      }
    } catch (err) {
      console.log(err);
    } finally {
      common.backdrop(false);
    }
  }
  async function getUserInfo() {
    try {
      common.backdrop(true);
      let response = await api.get(`/api/admin/get-info`);
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        let accountDetail = data || {};
        localStorage.setItem("role", accountDetail?.role);
        localStorage.setItem(
          "fullName",
          accountDetail?.firstName + " " + accountDetail?.lastName
        );
        navigate("/warehouse-management");
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    login,
  });

  return [context, domainInterface.current];
}
