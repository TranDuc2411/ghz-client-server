import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0001ActionList,
  A0001Context,
  createContext,
} from '../contexts/A0001Context';

export default function A0001ContextService() {
  const context = useStore()[A0001Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0001Context,
      type: A0001ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0001Context,
      type: A0001ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
