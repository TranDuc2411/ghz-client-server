import { InputRequired } from "@common/formConfig";
import { Button, Form, Input, Typography } from "antd";
import { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import A0001Domain from "../domains/A0001Domain";

export default function A0001Content() {
  const [, domain] = A0001Domain();
  const [isCaptcha, setIsCaptcha] = useState(false);
  const [form] = Form.useForm();

  async function handleLogin(values) {
    await domain.login(values);
  }

  return (
    <div className="w-full px-10 py-4 bg-white box-border">
      <div className="w-full flex justify-center mb-5">
        <h1 className="font-bold text-xl mb-1">Đăng nhập</h1>
      </div>
      <Form form={form} onFinish={handleLogin}>
        <div className="mb-5">
          <div className="text-base mb-1">Email</div>
          <Form.Item name="email" rules={[...InputRequired]}>
            <Input className="leading-7 bg-gray-100" placeholder="Nhập email" />
          </Form.Item>
        </div>
        <div className="mb-1">
          <div className="text-base mb-1">Mật khẩu</div>
          <Form.Item name="password" rules={[...InputRequired]}>
            <Input.Password
              className="leading-7 bg-gray-100"
              placeholder="Nhập mật khẩu"
            />
          </Form.Item>
        </div>

        <div className="w-full flex justify-center mb-2">
          {/* <ReCAPTCHA
            sitekey="6Ld_dlIpAAAAAHqv-uzbCY42O5_DVeuM9944DPN_"
            onChange={() => setIsCaptcha(true)}
          /> */}
        </div>

        <Form.Item className="w-full flex justify-center mb-1">
          <Button
            className="bg-blue-6 text-white ml-2 w-full"
            htmlType="submit"
            // disabled={!isCaptcha}
          >
            Đăng nhập
          </Button>
        </Form.Item>

        <div className="w-full flex justify-center">
          <Typography.Link className="italic">Quên mật khẩu ?</Typography.Link>
        </div>
      </Form>
    </div>
  );
}
