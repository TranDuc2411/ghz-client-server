import { io } from "socket.io-client";

const SOCKET_URL = import.meta.env.VITE_API_URL;

const token = localStorage.getItem("accessToken");

const socket = io(
  `${SOCKET_URL}/warehouse`,
  {
    extraHeaders: {
      Authorization: `Bearer ${token}`
    }
  }
);
socket.on('auth_error', (error) => {
  console.error('Lỗi - Authentication error:', error);
  alert(`Authentication error: ${error.error}`);
  socket.disconnect(); // Ngắt kết nối sau khi thông báo lỗi
});

export default socket;
