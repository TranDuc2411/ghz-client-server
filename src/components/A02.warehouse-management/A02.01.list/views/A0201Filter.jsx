import {
  Button,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  InputNumber,
  Radio,
  Row,
  Select,
  Space,
} from "antd";
const { RangePicker } = DatePicker;
export default function A0201Filter({ filter, setFilter, domain, context }) {
  const [form] = Form.useForm();

  async function search() {
    let params = await form.getFieldsValue(true);
    params.createDateFrom = params?.createDate ? params?.createDate[0] : null;
    params.createDateTo = params?.createDate ? params?.createDate[1] : null;
    await domain.getAllProduct(params);
  }
  return (
    <Drawer
      title="Bộ lọc"
      onClose={() => setFilter(false)}
      open={filter}
      extra={
        <Space>
          <Button>Làm mới</Button>
          <Button className="bg-blue-6 text-white" onClick={search}>
            Tìm kiếm
          </Button>
        </Space>
      }
    >
      <Form
        layout="vertical"
        form={form}
        initialValues={{
          isDelete: false,
          sellingPriceSort: 1,
          purchasePriceSort: 1,
          createDateSort: 1,
        }}
      >
        <Row gutter={16}>
          <Col span={24} className="mb-5">
            <Form.Item name="isDelete" label="">
              <Radio.Group>
                <Radio value={false}>Chưa bán</Radio>
                <Radio value={true}>Đã bán</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>

          <Col span={24} className="mb-2">
            <Form.Item name="productName" label="Tên sản phẩm">
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Col>
          <Col span={24} className="mb-2">
            <Form.Item name="state" label="Trạng thái">
              <Select
                placeholder="Chọn"
                options={context?.statusOption || []}
              />
            </Form.Item>
          </Col>

          <Col span={24} className="my-4 flex">
            <Form.Item label="Giá nhập">
              <div>
                <div className="flex">
                  <Form.Item className="mx-2">Từ</Form.Item>
                  <Form.Item name="purchasePriceFrom">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập từ"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                  <Form.Item className="mx-2">Đến</Form.Item>
                  <Form.Item
                    name="purchasePriceTo"
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (
                            !value ||
                            getFieldValue("purchasePriceFrom") <= value
                          ) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error("Giá nhập từ phải nhỏ hơn giá nhập đến")
                          );
                        },
                      }),
                    ]}
                  >
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập đến"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                </div>
                <div className="mt-2 ml-2">
                  <Form.Item name="purchasePriceSort">
                    <Radio.Group>
                      <Radio value={1}>Tăng</Radio>
                      <Radio value={-1}>Giảm</Radio>
                    </Radio.Group>
                  </Form.Item>
                </div>
              </div>
            </Form.Item>
          </Col>

          <Col span={24} className="my-4 flex">
            <Form.Item label="Giá bán đề xuất">
              <div>
                <div className="flex">
                  <Form.Item className="mx-2">Từ</Form.Item>
                  <Form.Item name="sellingPriceFrom">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập từ"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                  <Form.Item className="mx-2">Đến</Form.Item>
                  <Form.Item
                    name="sellingPriceTo"
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (
                            !value ||
                            getFieldValue("sellingPriceFrom") <= value
                          ) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error("Giá bán từ phải nhỏ hơn giá bán đến")
                          );
                        },
                      }),
                    ]}
                  >
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập đến"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                </div>
                <div className="mt-2 ml-2">
                  <Form.Item name="sellingPriceSort">
                    <Radio.Group>
                      <Radio value={1}>Tăng</Radio>
                      <Radio value={-1}>Giảm</Radio>
                    </Radio.Group>
                  </Form.Item>
                </div>
              </div>
            </Form.Item>
          </Col>

          <Col span={24} className="my-2">
            <Form.Item name={"createDate"} label="Ngày tạo">
              <RangePicker
                placeholder={["Ngày từ", "Ngày đến"]}
                format={["DD/MM/YYYY", "DD/MM/YYYY"]}
              />
            </Form.Item>
            <div className="mt-2 ml-2">
              <Form.Item name="createDateSort">
                <Radio.Group>
                  <Radio value={1}>Tăng</Radio>
                  <Radio value={-1}>Giảm</Radio>
                </Radio.Group>
              </Form.Item>
            </div>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
}
