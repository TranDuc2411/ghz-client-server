import {
  EditOutlined,
  FileSearchOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  configBgColor,
  formatCurrency,
  getScreenWidth,
  isSuperAdmin,
} from "@common/utils";
import { Button, Card, Input, Table, Tag, Typography } from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import A0201Domain from "../domains/A0201Domain";
import A0201Filter from "./A0201Filter";

export default function A0201Content() {
  const [context, domain] = A0201Domain();
  const [filter, setFilter] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    setDataSource(context?.dataTable);
  }, [context?.dataTable]);

  function getColumns() {
    let result = [
      {
        title: "stt",
        dataIndex: "index",
        key: "index",
        fixed: "left",
        width: 40,
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Tên máy",
        dataIndex: "productName",
        key: "productName",
        width: 100,
      },
      {
        title: "Thông tin máy",
        dataIndex: "configuration",
        width: 120,
      },
      {
        title: "Hình thức",
        dataIndex: "status",
        key: "status",
        width: 130,
        render: (value, record) => {
          return _.map(value, (el, index) => (
            <Tag key={record?.key + index} color={el?.color}>
              <div className={`text-wrap`}>{el?.title}</div>
            </Tag>
          ));
        },
      },
    ];
    if (isSuperAdmin()) {
      result = [
        ...result,
        ...[
          {
            title: "Giá nhập",
            dataIndex: "purchasePrice",
            key: "purchasePrice",
            width: 100,
            render: (value) => value && formatCurrency(value),
          },
        ],
      ];
    }

    result = [
      ...result,
      ...[
        {
          title: "Giá bán đề xuất",
          dataIndex: "sellingPrice",
          key: "sellingPrice",
          width: 100,
          render: (value) => value && formatCurrency(value),
        },
      ],
    ];
    result = [
      ...result,
      ...[
        {
          title: "Imei",
          dataIndex: "imei",
          key: "imei",
          width: 100,
        },
        {
          key: "action",
          width: 50,
          fixed: "right",
          render: (record) => (
            <Typography.Link
              onClick={() =>
                navigate(`/warehouse-management/update/${record?.id}`)
              }
            >
              <EditOutlined className="text-xl" />
            </Typography.Link>
          ),
        },
      ],
    ];
    return result || [];
  }
  return (
    <div className="h-auto w-auto">
      <A0201Filter
        filter={filter}
        setFilter={setFilter}
        domain={domain}
        context={context}
      />

      <Card
        className="w-auto"
        title={
          <span className={`break-words`}>Danh sách sản phẩm trong kho</span>
        }
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Input
              placeholder="Tìm kiếm"
              className="ml-2 w-[150px]"
              onChange={async (e) => {
                const value = e.target.value.toLowerCase().trim();
                let result = _.filter(
                  context.dataTable,
                  (el) =>
                    el?.imei?.toLowerCase().trim().includes(value) ||
                    el?.productName?.toLowerCase().trim().includes(value)
                );
                setDataSource(result);
              }}
            />
            <Button
              className="bg-grey-3 ml-2"
              onClick={() => setFilter(true)}
              icon={<FileSearchOutlined />}
            >
              {getScreenWidth() == "pc" && "Bộ lọc"}
            </Button>
            {isSuperAdmin() && (
              <Button
                className="bg-blue-6 text-white ml-2"
                onClick={() => navigate("/warehouse-management/create")}
                icon={<PlusOutlined />}
              >
                {getScreenWidth() == "pc" && "Tạo mới"}
              </Button>
            )}
          </div>
        }
      >
        <Table
          // style={{ transform: "scale(0.5)", transformOrigin: "top left" }}
          className="w-full h-90"
          dataSource={dataSource}
          bordered
          size="small"
          rowClassName={(record) => `${configBgColor(record?.state?.color)}`}
          scroll={{
            // x: "calc(700px + 50%)",
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}
