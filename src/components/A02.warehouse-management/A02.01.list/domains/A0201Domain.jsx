import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { Tag, message } from "antd";
import _ from "lodash";
import moment from "moment";
import { useEffect, useRef } from "react";
import A0201ContextService from "../services/A0201ContextService";
import socket from "../socket";

export default function A0201Domain() {
  const [context, contextService] = A0201ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const contextRef = useRef({
    dataTable: [],
    statusOption: [],
  });

  useEffect(() => {
    // Lắng nghe sự kiện "data_update"
    socket.on("data_update", (newData) => {
      const res = _.map(newData, (el, index) => ({
        ...el,
        key: index, // thêm ký cho mảng để không bị lỗi
      }));
      contextRef.current.dataTable = res || [];
      contextService.updateContext(contextRef.current);
    });
    return () => {
      socket.off("data_update"); // Khi component unmount, hủy lắng nghe
    };
  }, [socket]);
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    await getStatus();
    await getAllProduct();
  };

  async function getAllProduct(values) {
    try {
      common.backdrop(true);
      const params = {
        productName: null, // --> cái này chưa hoàn thiện để mặc định là null nhé!
        status: null, // --> cái này cũng chưa hoàn thiện, mặc định là null nhé !

        //--NHƯGX CÁI BÊN DƯỚI NÀY ĐỀU DÙNG ĐƯỢC--//
        isDelete: values?.isDelete || false,
        state: values?.state || null,
        createDateFrom: values?.createDateFrom
          ? moment(values?.createDateFrom?.$d).format("DD/MM/YYYY HH:mm:ss")
          : null, //DD/MM/YYYY HH:mm:ss
        createDateTo: values?.createDateTo
          ? moment(values?.createDateTo?.$d).format("DD/MM/YYYY HH:mm:ss")
          : null, //DD/MM/YYYY HH:mm:ss
        sellingPriceFrom: values?.sellingPriceFrom || null,
        sellingPriceTo: values?.sellingPriceTo || null,
        purchasePriceFrom: values?.purchasePriceFrom || null,
        purchasePriceTo: values?.purchasePriceTo || null,
        // các lựa chọn liên quan đến sắp xếp
        createDateSort: values?.createDateSort || 1, //1 là tăng dần -1 là giảm dần
        sellingPriceSort: values?.sellingPriceSort || 1, //1 là tăng dần -1 là giảm dần null sẽ mặc định là 1
        purchasePriceSort: values?.purchasePriceSort || 1, //1 là tăng dần -1 là giảm dần
      };

      let response = await api.post(`/api/admin/warehouse/filter`, params);
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataTable = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function getStatus() {
    try {
      let response = await api.get(
        `/api/admin/system-setting/state/getall?isDelete=false`
      );
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        let result = _.map(data, (el) => ({
          label: <Tag color={el?.color}>{el?.title}</Tag>,
          value: el?.stateCode,
        }));
        contextRef.current.statusOption = result || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    }
  }

  const domainInterface = useRef({
    initDomain,
    getAllProduct,
  });

  return [context, domainInterface.current];
}
