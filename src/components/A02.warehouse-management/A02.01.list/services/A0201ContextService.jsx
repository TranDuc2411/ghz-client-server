import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0201ActionList,
  A0201Context,
  createContext,
} from '../contexts/A0201Context';

export default function A0201ContextService() {
  const context = useStore()[A0201Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0201Context,
      type: A0201ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0201Context,
      type: A0201ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
