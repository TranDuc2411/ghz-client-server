import { createSlice } from "@core/store/store";

const A0201Context = "A0201Context";
const A0201ActionList = Object.freeze({
  UpdateContext: A0201Context + "/update",
  ResetContext: A0201Context + "/reset",
});

const A0201InitalState = {};

const A0201Actions = {};

A0201Actions[A0201ActionList.UpdateContext] = (state, payload) => {
  if (A0201Context != payload?.slice) {
    return state;
  }
  return { ...state, A0201Context: payload.data };
};

A0201Actions[A0201ActionList.ResetContext] = (state, payload) => {
  if (A0201Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0201InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0201Context, A0201Actions, {
    ...A0201InitalState,
    ...data,
  });
};
export { A0201ActionList, A0201Actions, A0201Context, createContext };
