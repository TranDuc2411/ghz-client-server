import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { Tag, message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import A0202ContextService from "../services/A0202ContextService";

export default function A0202Domain() {
  const [context, contextService] = A0202ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();
  const mode = _.includes(location.pathname, "update") ? "update" : "create";
  const { id } = useParams();

  const initContext = {
    mode: mode,
    statusOption: [],
    stickerOption: [],
    productNameOption: [],
    productDetail: {},
  };
  const contextRef = useRef(initContext);
  const initDomain = async () => {
    await contextService.initContext(initContext);

    if (!isSuperAdmin() && mode == "create") {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      await getStatus();
      await getSticker();
      await getProductName();

      if (mode == "update") {
        await getProduct(id);
      }
    }
  };

  //-----------------------------------------
  async function getProduct(id) {
    try {
      common.backdrop(true);
      let response = await api.get(
        `/api/admin/warehouses/product/detail/${id}`
      );
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        contextRef.current.productDetail = data || {};
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function getStatus() {
    try {
      common.backdrop(true);
      let response = await api.get(
        `/api/admin/system-setting/state/getall?isDelete=false`
      );
      const { data, status } = response.data || {};
      if (status == 201 && !!data) {
        let result = _.map(data, (el) => ({
          label: <Tag color={el?.color}>{el?.title}</Tag>,
          value: el?.stateCode,
        }));
        contextRef.current.statusOption = result || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function getSticker() {
    try {
      common.backdrop(true);
      let response = await api.get(
        `/api/admin/system-setting/status/getall?isDelete=false`
      );
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        let result = _.map(data, (el) => ({
          label: <Tag color={el?.color}>{el?.title}</Tag>,
          value: el?.id,
        }));
        contextRef.current.stickerOption = result || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function getProductName() {
    try {
      common.backdrop(true);
      let response = await api.get(
        `/api/admin/system-setting/product-name/getall?isDelete=false`
      );
      const { data, status } = response.data || {};
      if ((status == 200 || status == 201) && !!data) {
        let result = _.map(data, (el) => ({
          label: el.title,
          value: el.title,
        }));
        contextRef.current.productNameOption = result || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }

  async function createProduct(params, fileList) {
    try {
      common.backdrop(true);
      let productRequest = {
        imei: params.imei,
        productName: params.productName,
        configuration: params.configuration,
        originNote: params.originNote,
        notes: params.notes,
        status: params.status,
        state: params.state,
        sellingPrice: params.sellingPrice,
        purchasePrice: params.purchasePrice,
      };
      let resData = new FormData();
      resData.append("productRequest", JSON.stringify(productRequest));
      _.map(fileList, (item) => {
        resData.append("images", item?.originFileObj);
      });
      let response = await api({
        method: "post",
        url: `/api/supper-admin/warehouses/product/create`,
        data: resData,
        headers: { "Content-Type": "multipart/form-data" },
      });
      const { data, status } = response.data || {};
      if (status == 201 && !!data) {
        message.success(response.data.message);
        navigate("/warehouse-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateProduct(params, fileList) {
    try {
      common.backdrop(true);
      let resData = new FormData();
      let images = []; // list image giữ nguyên, ko xó
      _.map(fileList, (item) => {
        if (item?.url) {
          images.push(item?.url);
        } else {
          resData.append("images", item?.originFileObj);
        }
      });
      let productRequest = {
        imei: params.imei,
        productName: params.productName,
        configuration: params.configuration,
        originNote: params.originNote,
        notes: params.notes,
        status: params.status,
        state: params.state,
        sellingPrice: params.sellingPrice,
        purchasePrice: params.purchasePrice,
        images: images,
      };
      resData.append("productRequest", JSON.stringify(productRequest));
      let response = await api({
        method: "put",
        url: `/api/supper-admin/warehouses/product/update/` + id,
        data: resData,
        headers: { "Content-Type": "multipart/form-data" },
      });
      const { data, status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        navigate("/warehouse-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createProduct,
    updateProduct,
  });

  return [context, domainInterface.current];
}
