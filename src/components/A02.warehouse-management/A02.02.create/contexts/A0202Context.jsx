import { createSlice } from "@core/store/store";

const A0202Context = "A0202Context";
const A0202ActionList = Object.freeze({
  UpdateContext: A0202Context + "/update",
  ResetContext: A0202Context + "/reset",
});

const A0202InitalState = {};

const A0202Actions = {};

A0202Actions[A0202ActionList.UpdateContext] = (state, payload) => {
  if (A0202Context != payload?.slice) {
    return state;
  }
  return { ...state, A0202Context: payload.data };
};

A0202Actions[A0202ActionList.ResetContext] = (state, payload) => {
  if (A0202Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0202InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0202Context, A0202Actions, {
    ...A0202InitalState,
    ...data,
  });
};
export { A0202ActionList, A0202Actions, A0202Context, createContext };
