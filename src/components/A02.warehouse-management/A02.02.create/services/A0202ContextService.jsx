import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0202ActionList,
  A0202Context,
  createContext,
} from '../contexts/A0202Context';

export default function A0202ContextService() {
  const context = useStore()[A0202Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0202Context,
      type: A0202ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0202Context,
      type: A0202ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
