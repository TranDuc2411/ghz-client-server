import { EditOutlined } from "@ant-design/icons";
import {
  InputMax,
  InputNumberRequired,
  InputRequired,
  InputTextAreaMax,
} from "@common/formConfig";
import { isSuperAdmin } from "@common/utils";
import { Button, Card, Form, Input, InputNumber, Select, Space } from "antd";
import _ from "lodash";
import moment from "moment/moment";
import { useEffect, useState } from "react";
import { FaCheckCircle } from "react-icons/fa";
import { IoCaretBackOutline } from "react-icons/io5";
import A0202Domain from "../domains/A0202Domain";
import A0202Upload from "./A0202Upload";

export default function A0202Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0202Domain();
  const { productDetail, mode } = context || {};
  const [updateMode, setUpdateMode] = useState(true);
  const [fileList, setFileList] = useState([
    // {
    //   uid: "-1",
    //   name: "image.png",
    //   status: "done",
    //   url: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
    // },
  ]);

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    let images = [];
    _.map(productDetail?.imgUrl, (item, index) => {
      images.push({
        uid: "id" + index,
        name: "image.png",
        status: "done",
        url: item,
      });
    });
    setFileList(images);
    if (mode == "update") {
      let statusList = _.map(productDetail?.status, (el) => el?.id);
      form.setFieldsValue({
        imei: productDetail?.imei || null,
        productName: productDetail?.productName,
        configuration: productDetail?.configuration,
        originNote: productDetail?.originNote,
        notes: productDetail?.notes,
        status: statusList,
        state: productDetail?.state?.stateCode,
        sellingPrice: productDetail?.sellingPrice,
        purchasePrice: productDetail?.purchasePrice,
        createdDate: moment(productDetail?.createdDate).format("DD/MM/YYYY"),
      });
    }  else {
      form.resetFields()
    }
  }, [productDetail]);

  async function createProduct(values) {
    await domain.createProduct(values, fileList);
  }

  async function updateProduct() {
    await form.validateFields();
    let values = await form.getFieldsValue(true);
    await domain.updateProduct(values, fileList);
  }

  return (
    <div className="h-auto w-auto">
      <Card
        title={mode == "update" ? "Chỉnh sửa sản phẩm" : "Tạo mới sản phẩm"}
        className="w-full"
        extra={
          <Button danger onClick={() => setUpdateMode(false)}>
            Nhập lại sản phẩm
          </Button>
        }
      >
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
          onFinish={createProduct}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Imei"
              name="imei"
              rules={[ ...InputMax]}
              // rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Tên sản phẩm"
              name="productName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Select
                showSearch
                placeholder="Chọn"
                disabled={mode != "create" && updateMode}
                options={context?.productNameOption}
                filterOption={(input, option) =>
                  (option?.label ?? "")
                    .toLowerCase()
                    .includes(input.toLowerCase())
                }
              />
            </Form.Item>
            <Form.Item
              label="Thông tin máy"
              name="configuration"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Nguồn gốc"
              name="originNote"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Giá nhập"
              name="sellingPrice"
              rules={[...InputNumberRequired]}
              className="mt-2-custom"
            >
              <InputNumber
                placeholder="Nhập thông tin"
                className="w-[200px]"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            {isSuperAdmin() && (
              <Form.Item
                label="Giá bán đề xuất"
                name="purchasePrice"
                rules={[...InputNumberRequired]}
                className="mt-2-custom"
              >
                <InputNumber
                  placeholder="Nhập thông tin"
                  className="w-[200px]"
                  min={0}
                  max={10000000000000}
                  controls={false}
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  disabled={mode != "create" && updateMode}
                />
              </Form.Item>
            )}
            <Form.Item label="Trạng thái" name="state" className="mt-2-custom">
              <Select
                placeholder="Chọn thông tin"
                options={context?.statusOption}
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item label="Hình thức" name="status" className="mt-2-custom">
              <Select
                placeholder="Chọn thông tin"
                options={context?.stickerOption}
                mode="multiple"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            {mode !== "create" && (
              <Form.Item
                label="Ngày tạo"
                name="createdDate"
                className="mt-2-custom"
              >
                <Input disabled={true} />
              </Form.Item>
            )}
            <Form.Item
              label="Ghi chú"
              name="notes"
              rules={[...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item label="Tải ảnh">
              <A0202Upload
                fileList={fileList}
                setFileList={setFileList}
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>

            {isSuperAdmin() && (
              <>
                {mode == "create" && (
                  <Form.Item label=" ">
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="bg-blue-6 text-white"
                      icon={<FaCheckCircle />}
                    >
                      Xác nhận tạo mới
                    </Button>
                  </Form.Item>
                )}
                {mode == "update" && (
                  <>
                    {updateMode ? (
                      <Form.Item label=" ">
                        <Button
                          type="primary"
                          // htmlType="submit"
                          className="bg-blue-6 text-white"
                          onClick={() => setUpdateMode(false)}
                          icon={<EditOutlined className="text-xl text-white" />}
                        >
                          Chỉnh sửa
                        </Button>
                      </Form.Item>
                    ) : (
                      <Form.Item label=" " className="">
                        <Button
                          type="primary"
                          className="bg-grey-3 ml-2 text-black mr-4"
                          onClick={() => setUpdateMode(true)}
                          icon={<IoCaretBackOutline />}
                        >
                          Quay lại
                        </Button>
                        <Button
                          type="primary"
                          className="bg-blue-6 text-white"
                          onClick={() => updateProduct()}
                          icon={<FaCheckCircle />}
                        >
                          Xác nhận chỉnh sửa
                        </Button>
                      </Form.Item>
                    )}
                  </>
                )}
              </>
            )}
          </Space>
        </Form>
      </Card>
    </div>
  );
}
