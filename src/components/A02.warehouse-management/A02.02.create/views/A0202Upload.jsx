import { Upload } from "antd";
import ImgCrop from "antd-img-crop";
import "./Upload.scss";

const A0202Upload = ({ fileList, setFileList, disabled }) => {
  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const onPreview = async (file) => {
    // Kiểm tra xem file đã được tải lên hoàn toàn chưa
    if (!file.url && !file.preview) {
      file.preview = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
  
    // Mở cửa sổ xem trước
    const image = new Image();
    image.src = file.url || file.preview;
    const imgWindow = window.open(image.src);
    imgWindow?.document.write(image.outerHTML);
  };
  return (
    <>
      <Upload
        listType="picture-card"
        fileList={fileList}
        onChange={onChange}
        onPreview={onPreview}
        disabled={disabled}
        // resize={{ width: 1920, height: 1080, quality: 0.9, crop: false }}
      >
        {!disabled && fileList.length < 6 && "+ Upload"}
      </Upload>
    </>
  );
};
export default A0202Upload;
