import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { InputMax, InputRequired, InputTextAreaMax } from "@common/formConfig";
import { configBgColor, getScreenWidth } from "@common/utils";
import { Button, Card, Form, Input, Modal, Space, Table, Tag } from "antd";
import { useEffect, useState } from "react";
import { BlockPicker } from "react-color";
import { IoRefresh } from "react-icons/io5";
import A0301Domain from "../domains/A0301Domain";

export default function A0301Content() {
  const [context, domain] = A0301Domain();
  const [isStateCreateModalOpen, setIsStateCreateModalOpen] = useState(false);
  const [isStateDeleteModalOpen, setIsStateDeleteModalOpen] = useState(false);
  const [isStateRestoreModalOpen, setIsStateRestoreModalOpen] = useState(false);
  const [isStateUpdateModalOpen, setIsStateUpdateModalOpen] = useState(false);
  const [selectedId, setselectedId] = useState(null);

  useEffect(() => {
    domain.initDomain();
  }, []);

  async function createStatus(value) {
    await domain.createStatus(value);
  }
  async function deleteStatus() {
    await domain.deleteStatus(selectedId?.id);
  }
  async function restoreStatus() {
    await domain.restoreStatus(selectedId?.id);
  }
  async function updateStatus(value) {
    await domain.updateStatus(value);
  }

  function getColumns() {
    let result = [
      {
        title: "Stt",
        dataIndex: "index",
        width: "7%",
        render: (value, record, index) => index + 1,
      },
      {
        title: "Tên trạng thái",
        dataIndex: "title",
        width: "20%",
      },
      {
        title: "Mã màu",
        dataIndex: "color",
        width: "10%",
      },
      {
        title: "Mô tả",
        dataIndex: "description",
        width: "33%",
      },
      {
        title: "Hiệu lực",
        dataIndex: "isDelete",
        width: "15%",
        render: (value) => (value ? "Hết hiệu lực" : "Còn hiệu lực"),
      },
      {
        title: "",
        width: "15%",
        render: (value, record, index) => (
          <div className="flex flex-nowrap">
            <Tag
              color="blue"
              onClick={() => {
                setIsStateUpdateModalOpen(true);
                setselectedId(record);
              }}
            >
              <EditOutlined className="text-xl text-blue-6" />
            </Tag>
            {record?.isDelete ? (
              <Tag
                color="blue"
                className="px-2"
                onClick={() => {
                  setIsStateRestoreModalOpen(true);
                  setselectedId(record);
                }}
              >
                <IoRefresh className="text-xl text-blue-6" />
              </Tag>
            ) : (
              <Tag
                color="red"
                onClick={() => {
                  setIsStateDeleteModalOpen(true);
                  setselectedId(record);
                }}
              >
                <DeleteOutlined className="text-xl text-red-6" />
              </Tag>
            )}
          </div>
        ),
      },
    ];
    return result || [];
  }
  return (
    <div className="h-auto w-auto">
      <Card
        className="w-auto"
        title={<span className={`break-words`}>Danh sách trạng thái</span>}
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Button
              className="bg-blue-6 text-white ml-2"
              onClick={() => setIsStateCreateModalOpen(true)}
              icon={<PlusOutlined />}
            >
              {getScreenWidth() == "pc" && "Tạo mới"}
            </Button>
          </div>
        }
      >
        <ModalCreate
          open={isStateCreateModalOpen}
          handleCancel={() => setIsStateCreateModalOpen(false)}
          handleOk={createStatus}
        />
        <ModalDelete
          open={isStateDeleteModalOpen}
          handleCancel={() => setIsStateDeleteModalOpen(false)}
          handleOk={deleteStatus}
        />
        <ModalRestore
          open={isStateRestoreModalOpen}
          handleCancel={() => setIsStateRestoreModalOpen(false)}
          handleOk={restoreStatus}
        />
        <ModalUpdate
          open={isStateUpdateModalOpen}
          handleCancel={() => setIsStateUpdateModalOpen(false)}
          handleOk={updateStatus}
          selectedId={selectedId}
        />
        <Table
          className="w-full h-90"
          dataSource={context?.dataStateList}
          bordered
          size="small"
          rowClassName={(record) => `${configBgColor(record?.color)}`}
          scroll={{
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}

function ModalDelete({ open, handleCancel, handleOk }) {
  async function handleSubmit() {
    await handleOk();
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-red-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <div className="text-lg font-bold">
        Bạn có chắc chắn muốn hủy hiệu lực bản ghi?
      </div>
    </Modal>
  );
}
function ModalRestore({ open, handleCancel, handleOk }) {
  async function handleSubmit() {
    await handleOk();
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <div className="text-lg font-bold">
        Bạn có chắc chắn muốn khôi phục hiệu lực bản ghi?
      </div>
    </Modal>
  );
}
function ModalCreate({ open, handleCancel, handleOk }) {
  const [form] = Form.useForm();
  const [color, setColor] = useState("#5DA0F6");
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      title: values?.title,
      color: color.toUpperCase(),
      description: values?.description,
    });
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Tạo mới trạng thái" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên trạng thái"
              name="title"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
            <Form.Item
              label="Mã màu"
              name="color"
              className="mt-2-custom"
              required
            >
              <BlockPicker
                className="shadow-md w-full"
                color={color}
                onChangeComplete={(e) => {
                  setColor(e.hex);
                }}
                colors={[
                  "#5DA0F6", // bg-blue-5
                  "#FFA940", // bg-orange-5
                  "#FF4D4F", // bg-red-5
                  "#4DD077", // bg-green-5
                  "#BFBFBF", // bg-grey-6
                  "#36CFC9", // bg-cyan-5
                  "#FFD700", // bg-yellow-1
                  "#EE82EE", // bg-violet-1
                ]}
              />
            </Form.Item>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}

function ModalUpdate({ open, handleCancel, handleOk, selectedId }) {
  const [form] = Form.useForm();
  const [color, setColor] = useState(null);
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      id: selectedId?.id,
      title: values?.title,
      color: color.toUpperCase(),
      description: values?.description,
    });
    handleCancel();
  }

  useEffect(() => {
    form.setFieldsValue({
      title: selectedId?.title,
      description: selectedId?.description,
    });
    setColor(selectedId?.color);
  }, [selectedId]);

  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Chỉnh sửa trạng thái" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên trạng thái"
              name="title"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
            <Form.Item
              label="Mã màu"
              name="color"
              className="mt-2-custom"
              required
            >
              <BlockPicker
                className="shadow-md w-full"
                color={color}
                onChangeComplete={(e) => {
                  setColor(e.hex);
                }}
                colors={[
                  "#5DA0F6", // bg-blue-5
                  "#FFA940", // bg-orange-5
                  "#FF4D4F", // bg-red-5
                  "#4DD077", // bg-green-5
                  "#BFBFBF", // bg-grey-6
                  "#36CFC9", // bg-cyan-5
                  "#FFD700", // bg-yellow-1
                  "#EE82EE", // bg-violet-1
                ]}
              />
            </Form.Item>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}
