import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0301ActionList,
  A0301Context,
  createContext,
} from '../contexts/A0301Context';

export default function A0301ContextService() {
  const context = useStore()[A0301Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0301Context,
      type: A0301ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0301Context,
      type: A0301ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
