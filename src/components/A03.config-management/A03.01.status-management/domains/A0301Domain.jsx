import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import A0301ContextService from "../services/A0301ContextService";

export default function A0301Domain() {
  const [context, contextService] = A0301ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const navigate = useNavigate();
  const contextRef = useRef({
    dataStateList: [],
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      await getAllStatus();
    }
  };

  async function getAllStatus() {
    try {
      common.backdrop(true);
      let response = await api.get("/api/admin/system-setting/state/getall");
      const { data, status } = response.data || {};
      if (status == 201 || status == 200) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataStateList = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  async function createStatus(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "post",
        url: `/api/supper-admin/system-setting/state/create`,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStatus();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateStatus(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "put",
        url: `/api/supper-admin/system-setting/state/update/` + params?.id,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStatus();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function deleteStatus(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url: `/api/supper-admin/system-setting/state/delete/` + id,
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStatus();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function restoreStatus(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url: `/api/supper-admin/system-setting/state/restore/` + id,
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStatus();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createStatus,
    deleteStatus,
    updateStatus,
    restoreStatus,
  });

  return [context, domainInterface.current];
}
