import { createSlice } from "@core/store/store";

const A0301Context = "A0301Context";
const A0301ActionList = Object.freeze({
  UpdateContext: A0301Context + "/update",
  ResetContext: A0301Context + "/reset",
});

const A0301InitalState = {};

const A0301Actions = {};

A0301Actions[A0301ActionList.UpdateContext] = (state, payload) => {
  if (A0301Context != payload?.slice) {
    return state;
  }
  return { ...state, A0301Context: payload.data };
};

A0301Actions[A0301ActionList.ResetContext] = (state, payload) => {
  if (A0301Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0301InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0301Context, A0301Actions, {
    ...A0301InitalState,
    ...data,
  });
};
export { A0301ActionList, A0301Actions, A0301Context, createContext };
