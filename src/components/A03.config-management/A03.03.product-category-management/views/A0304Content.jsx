import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { InputMax, InputRequired } from "@common/formConfig";
import { getScreenWidth } from "@common/utils";
import { Button, Card, Form, Input, Modal, Space, Table, Tag } from "antd";
import { useEffect, useState } from "react";
import A0304Domain from "../domains/A0304Domain";
import { render } from "react-dom";

export default function A0304Content() {
  const [context, domain] = A0304Domain();
  const [isStateCreateModalOpen, setIsStateCreateModalOpen] = useState(false);
  const [isStateDeleteModalOpen, setIsStateDeleteModalOpen] = useState(false);
  const [isStateUpdateModalOpen, setIsStateUpdateModalOpen] = useState(false);
  const [selectedId, setselectedId] = useState(null);

  useEffect(() => {
    domain.initDomain();
  }, []);

  async function createProductName(value) {
    await domain.createProductName(value);
  }
  async function deleteProductName() {
    await domain.deleteProductName(selectedId?.id);
  }
  async function updateProductName(value) {
    await domain.updateProductName(value);
  }

  function getColumns() {
    let result = [
      {
        title: "STT",
        dataIndex: "index",
        width: "10%",
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Loại sản phẩm",
        dataIndex: "title",
        width: "20%",
      },
      {
        title: "Mô tả",
        dataIndex: "description",
        width: "20%",
      },
      {
        title: "Trạng thái",
        dataIndex: "isDelete",
        width: "20%%",
        render: (value) => (value ? "Đã xóa" : "Đang hoạt động"),
      },
      {
        title: "",
        width: "15%",
        render: (value, record, _index) => (
          <div className="flex flex-nowrap">
            <Tag
              color="blue"
              className="px-2"
              onClick={() => {
                setIsStateUpdateModalOpen(true);
                setselectedId(record);
              }}
            >
              <EditOutlined className="text-xl text-blue-6" />
            </Tag>
            <Tag
              color="red"
              className="px-2"
              onClick={() => {
                setIsStateDeleteModalOpen(true);
                setselectedId(record);
              }}
            >
              <DeleteOutlined className="text-xl text-red-6" />
            </Tag>
          </div>
        ),
      },
    ];
    return result || [];
  }
  return (
    
    <div className="h-auto w-auto">
      <Card
        className="w-auto"
        title={
          <span
            className={`break-words`}
          >
            Danh sách tên sản phẩm
          </span>
        }
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Button
              className="bg-blue-6 text-white ml-2"
              onClick={() => setIsStateCreateModalOpen(true)}
              icon={<PlusOutlined />}
            >
              {getScreenWidth() == "pc" && "Tạo mới"}
            </Button>
          </div>
        }
      >
        <ModalCreate
          open={isStateCreateModalOpen}
          handleCancel={() => setIsStateCreateModalOpen(false)}
          handleOk={createProductName}
        />
        <ModalDelete
          open={isStateDeleteModalOpen}
          handleCancel={() => setIsStateDeleteModalOpen(false)}
          handleOk={deleteProductName}
        />
        <ModalUpdate
          open={isStateUpdateModalOpen}
          handleCancel={() => setIsStateUpdateModalOpen(false)}
          handleOk={updateProductName}
          selectedId={selectedId}
        />
        <Table
          className="w-full h-90"
          dataSource={context?.dataStateList}
          bordered
          size="small"
          scroll={{
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}

function ModalDelete({ open, handleCancel, handleOk }) {
  async function handleSubmit() {
    await handleOk();
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-red-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <div className="text-lg font-bold">
        Bạn có chắc chắn muốn xóa bản ghi?
      </div>
    </Modal>
  );
}
function ModalCreate({ open, handleCancel, handleOk }) {
  const [form] = Form.useForm();
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      title: values?.title,
    });
    
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Tạo mới tên sản phẩm" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Danh mục"
              name="title"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}
function ModalUpdate({ open, handleCancel, handleOk, selectedId }) {
  const [form] = Form.useForm();
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      id: selectedId?.id,
      name: values?.name,
    });
    handleCancel();
  }

  useEffect(() => {
    form.setFieldsValue({
      name: selectedId?.title,
    });
  }, [selectedId]);

  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Chỉnh sửa tên sản phẩm" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên sản phẩm"
              name="name"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}
