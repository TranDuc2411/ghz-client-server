import { createSlice } from "@core/store/store";

const A0304Context = "A0304Context";
const A0304ActionList = Object.freeze({
  UpdateContext: A0304Context + "/update",
  ResetContext: A0304Context + "/reset",
});

const A0304InitalState = {};

const A0304Actions = {};

A0304Actions[A0304ActionList.UpdateContext] = (state, payload) => {
  if (A0304Context != payload?.slice) {
    return state;
  }
  return { ...state, A0304Context: payload.data };
};

A0304Actions[A0304ActionList.ResetContext] = (state, payload) => {
  if (A0304Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0304InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0304Context, A0304Actions, {
    ...A0304InitalState,
    ...data,
  });
};
export { A0304ActionList, A0304Actions, A0304Context, createContext };
