import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0304ActionList,
  A0304Context,
  createContext,
} from '../contexts/A0304Context';

export default function A0304ContextService() {
  const context = useStore()[A0304Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0304Context,
      type: A0304ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0304Context,
      type: A0304ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
