import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import A0303ContextService from "../services/A0303ContextService";

export default function A0303Domain() {
  const [context, contextService] = A0303ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const navigate = useNavigate();
  const contextRef = useRef({
    dataStateList: [],
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      await getAllProductName();
    }
  };

  async function getAllProductName() {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/supper-admin/system-setting/product-name/getall?isDelete=false`,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataStateList = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function createProductName(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "post",
        url: `/api/supper-admin/system-setting/product-name/create`,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        await getAllProductName();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateProductName(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url:
          `/api/supper-admin/system-setting/product-name/update/` + params?.id,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        await getAllProductName();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function deleteProductName(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url: `/api/supper-admin/system-setting/product-name/delete/` + id,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        await getAllProductName();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createProductName,
    deleteProductName,
    updateProductName,
  });

  return [context, domainInterface.current];
}
