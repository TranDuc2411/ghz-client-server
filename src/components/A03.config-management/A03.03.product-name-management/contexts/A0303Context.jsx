import { createSlice } from "@core/store/store";

const A0303Context = "A0303Context";
const A0303ActionList = Object.freeze({
  UpdateContext: A0303Context + "/update",
  ResetContext: A0303Context + "/reset",
});

const A0303InitalState = {};

const A0303Actions = {};

A0303Actions[A0303ActionList.UpdateContext] = (state, payload) => {
  if (A0303Context != payload?.slice) {
    return state;
  }
  return { ...state, A0303Context: payload.data };
};

A0303Actions[A0303ActionList.ResetContext] = (state, payload) => {
  if (A0303Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0303InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0303Context, A0303Actions, {
    ...A0303InitalState,
    ...data,
  });
};
export { A0303ActionList, A0303Actions, A0303Context, createContext };
