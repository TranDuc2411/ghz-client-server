import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0303ActionList,
  A0303Context,
  createContext,
} from '../contexts/A0303Context';

export default function A0303ContextService() {
  const context = useStore()[A0303Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0303Context,
      type: A0303ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0303Context,
      type: A0303ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
