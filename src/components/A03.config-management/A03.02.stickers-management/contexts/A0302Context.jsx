import { createSlice } from "@core/store/store";

const A0302Context = "A0302Context";
const A0302ActionList = Object.freeze({
  UpdateContext: A0302Context + "/update",
  ResetContext: A0302Context + "/reset",
});

const A0302InitalState = {};

const A0302Actions = {};

A0302Actions[A0302ActionList.UpdateContext] = (state, payload) => {
  if (A0302Context != payload?.slice) {
    return state;
  }
  return { ...state, A0302Context: payload.data };
};

A0302Actions[A0302ActionList.ResetContext] = (state, payload) => {
  if (A0302Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0302InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0302Context, A0302Actions, {
    ...A0302InitalState,
    ...data,
  });
};
export { A0302ActionList, A0302Actions, A0302Context, createContext };
