import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { InputMax, InputRequired, InputTextAreaMax } from "@common/formConfig";
import { STICKER_LIST, STICKER_OPTION, getScreenWidth } from "@common/utils";
import {
  Button,
  Card,
  Form,
  Input,
  Modal,
  Select,
  Space,
  Table,
  Tag,
} from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import { IoRefresh } from "react-icons/io5";
import A0302Domain from "../domains/A0302Domain";

export default function A0302Content() {
  const [context, domain] = A0302Domain();
  const [isStateCreateModalOpen, setIsStateCreateModalOpen] = useState(false);
  const [isStateDeleteModalOpen, setIsStateDeleteModalOpen] = useState(false);
  const [isStateRestoreModalOpen, setIsStateRestoreModalOpen] = useState(false);
  const [isStateUpdateModalOpen, setIsStateUpdateModalOpen] = useState(false);
  const [selectedId, setselectedId] = useState(null);

  useEffect(() => {
    domain.initDomain();
  }, []);

  async function createStickers(value) {
    await domain.createStickers(value);
  }
  async function deleteStickers() {
    await domain.deleteStickers(selectedId?.id);
  }
  async function updateStickers(value) {
    await domain.updateStickers(value);
  }
  async function restoreStickers() {
    await domain.restoreStickers(selectedId?.id);
  }

  function getColumns() {
    let result = [
      {
        title: "STT",
        dataIndex: "index",
        width: "10%",
        render: (value, record, index) => index + 1,
      },
      {
        title: "Tên nhãn dán",
        dataIndex: "title",
        width: "20%",
        render: (value, record, index) =>
          value && (
            <Tag
              color={
                _.includes(STICKER_LIST, record?.color)
                  ? record?.color
                  : "default"
              }
            >
              {value}
            </Tag>
          ),
      },
      {
        title: "Mô tả",
        dataIndex: "description",
        width: "40%",
      },
      {
        title: "Hiệu lực",
        dataIndex: "isDelete",
        width: "15%",
        render: (value) => (value ? "Hết hiệu lực" : "Còn hiệu lực"),
      },
      {
        title: "",
        width: "15%",
        render: (value, record, index) => (
          <div className="flex flex-nowrap">
            <Tag
              color="blue"
              className="px-2"
              onClick={() => {
                setIsStateUpdateModalOpen(true);
                setselectedId(record);
              }}
            >
              <EditOutlined className="text-xl text-blue-6" />
            </Tag>
            {record?.isDelete ? (
              <Tag
                color="blue"
                className="px-2"
                onClick={() => {
                  setIsStateRestoreModalOpen(true);
                  setselectedId(record);
                }}
              >
                <IoRefresh className="text-xl text-blue-6" />
              </Tag>
            ) : (
              <Tag
                color="red"
                className="px-2"
                onClick={() => {
                  setIsStateDeleteModalOpen(true);
                  setselectedId(record);
                }}
              >
                <DeleteOutlined className="text-xl text-red-6" />
              </Tag>
            )}
          </div>
        ),
      },
    ];
    return result || [];
  }
  return (
    <div className="h-auto w-auto">
      <Card
        className="w-auto"
        title={<span className={`break-words`}>Danh sách nhãn dán</span>}
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Button
              className="bg-blue-6 text-white ml-2"
              onClick={() => setIsStateCreateModalOpen(true)}
              icon={<PlusOutlined />}
            >
              {getScreenWidth() == "pc" && "Tạo mới"}
            </Button>
          </div>
        }
      >
        <ModalCreate
          open={isStateCreateModalOpen}
          handleCancel={() => setIsStateCreateModalOpen(false)}
          handleOk={createStickers}
        />
        <ModalDelete
          open={isStateDeleteModalOpen}
          handleCancel={() => setIsStateDeleteModalOpen(false)}
          handleOk={deleteStickers}
        />
        <ModalRestore
          open={isStateRestoreModalOpen}
          handleCancel={() => setIsStateRestoreModalOpen(false)}
          handleOk={restoreStickers}
        />

        <ModalUpdate
          open={isStateUpdateModalOpen}
          handleCancel={() => setIsStateUpdateModalOpen(false)}
          handleOk={updateStickers}
          selectedId={selectedId}
        />
        <Table
          className="w-full h-90"
          dataSource={context?.dataStateList}
          bordered
          size="small"
          scroll={{
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}

function ModalDelete({ open, handleCancel, handleOk }) {
  async function handleSubmit() {
    await handleOk();
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-red-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <div className="text-lg font-bold">
        Bạn có chắc chắn muốn hủy hiệu lực bản ghi?
      </div>
    </Modal>
  );
}
function ModalRestore({ open, handleCancel, handleOk }) {
  async function handleSubmit() {
    await handleOk();
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <div className="text-lg font-bold">
        Bạn có chắc chắn muốn làm mới hiệu lực bản ghi?
      </div>
    </Modal>
  );
}
function ModalCreate({ open, handleCancel, handleOk }) {
  const [form] = Form.useForm();
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      title: values?.title,
      color: values?.color,
      description: values?.description,
    });
    handleCancel();
  }
  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Tạo mới nhãn dán" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên nhãn dán"
              name="title"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
            <Form.Item
              label="Mã màu nhãn dán"
              name="color"
              className="mt-2-custom"
              rules={[...InputRequired]}
            >
              <Select options={STICKER_OPTION || []} placeholder="Chọn" />
            </Form.Item>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}

function ModalUpdate({ open, handleCancel, handleOk, selectedId }) {
  const [form] = Form.useForm();
  async function handleSubmit() {
    await form.validateFields();
    let values = await form.getFieldsValue();
    await handleOk({
      id: selectedId?.id,
      title: values?.title,
      color: values?.color,
      description: values?.description,
    });
    handleCancel();
  }
  useEffect(() => {
    form.setFieldsValue({
      title: selectedId?.title,
      description: selectedId?.description,
      color: selectedId?.color,
    });
  }, [selectedId]);

  return (
    <Modal
      open={open}
      onCancel={handleCancel}
      footer={[
        <Button key={1} onClick={handleCancel}>
          Quay lại
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={handleSubmit}
        >
          Xác nhận
        </Button>,
      ]}
      closeIcon={null}
    >
      <Card title="Chỉnh sửa nhãn dán" className="w-full">
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên nhãn dán"
              name="title"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
            <Form.Item
              label="Mã màu nhãn dán"
              name="color"
              className="mt-2-custom"
              rules={[...InputRequired]}
            >
              <Select options={STICKER_OPTION || []} placeholder="Chọn" />
            </Form.Item>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[...InputRequired, ...InputTextAreaMax]}
              className="mt-2-custom"
            >
              <Input.TextArea placeholder="Nhập thông tin" />
            </Form.Item>
          </Space>
        </Form>
      </Card>
    </Modal>
  );
}
