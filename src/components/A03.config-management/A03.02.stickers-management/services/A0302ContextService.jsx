import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0302ActionList,
  A0302Context,
  createContext,
} from '../contexts/A0302Context';

export default function A0302ContextService() {
  const context = useStore()[A0302Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0302Context,
      type: A0302ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0302Context,
      type: A0302ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
