import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import A0302ContextService from "../services/A0302ContextService";

export default function A0302Domain() {
  const [context, contextService] = A0302ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();
  const contextRef = useRef({
    dataStateList: [],
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      await getAllStickers();
    }
  };

  async function getAllStickers() {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/system-setting/status/getall`,
      });
      const { data, status } = response.data || {};
      if (status == 201 || status == 200) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataStateList = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  async function createStickers(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "post",
        url: `/api/supper-admin/system-setting/status/create`,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStickers();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateStickers(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "put",
        url: `/api/supper-admin/system-setting/status/update/` + params?.id,
        data: params || {},
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStickers();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function deleteStickers(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url: `/api/supper-admin/system-setting/status/delete/` + id,
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStickers();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function restoreStickers(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "patch",
        url: `/api/supper-admin/system-setting/status/restore/` + id,
      });
      const { status } = response.data || {};
      if (status == 201 || status == 200) {
        message.success(response.data.message);
        await getAllStickers();
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createStickers,
    deleteStickers,
    updateStickers,
    restoreStickers,
  });

  return [context, domainInterface.current];
}
