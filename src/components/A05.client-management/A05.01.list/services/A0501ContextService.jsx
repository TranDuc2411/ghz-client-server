import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0501ActionList,
  A0501Context,
  createContext,
} from '../contexts/A0501Context';

export default function A0501ContextService() {
  const context = useStore()[A0501Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0501Context,
      type: A0501ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0501Context,
      type: A0501ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
