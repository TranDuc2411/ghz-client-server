import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import A0501ContextService from "../services/A0501ContextService";

export default function A0501Domain() {
  const [context, contextService] = A0501ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();

  const contextRef = useRef({
    dataTable: [],
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/warehouse-management");
    } else {
      await getAllAccount();
    }
  };

  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  async function getAllAccount() {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/crm/client/getAll`,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataTable = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  const domainInterface = useRef({
    initDomain,
  });

  return [context, domainInterface.current];
}
