import { Button, Col, Drawer, Form, Input, Row, Select, Space } from "antd";

export default function A0501Filter({ filter, setFilter }) {
  return (
    <Drawer
      title="Bộ lọc"
      onClose={() => setFilter(false)}
      open={filter}
      extra={
        <Space>
          <Button>Làm mới</Button>
          <Button className="bg-blue-6 text-white">Tìm kiếm</Button>
        </Space>
      }
    >
      <Form layout="vertical">
        <Row gutter={16}>
          <Col span={24} className="mb-2">
            <Form.Item name="name" label="Tên">
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Col>

          <Col span={24} className="my-2">
            <Form.Item name="name" label="Số điện thoại">
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Col>

          <Col span={24} className="my-2">
            <Form.Item name="name" label="Vai trò">
              <Select placeholder="Chọn" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
}
