import { createSlice } from "@core/store/store";

const A0501Context = "A0501Context";
const A0501ActionList = Object.freeze({
  UpdateContext: A0501Context + "/update",
  ResetContext: A0501Context + "/reset",
});

const A0501InitalState = {};

const A0501Actions = {};

A0501Actions[A0501ActionList.UpdateContext] = (state, payload) => {
  if (A0501Context != payload?.slice) {
    return state;
  }
  return { ...state, A0501Context: payload.data };
};

A0501Actions[A0501ActionList.ResetContext] = (state, payload) => {
  if (A0501Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0501InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0501Context, A0501Actions, {
    ...A0501InitalState,
    ...data,
  });
};
export { A0501ActionList, A0501Actions, A0501Context, createContext };
