import {
  EditOutlined,
  MinusCircleOutlined,
  PlusCircleOutlined,
} from "@ant-design/icons";
import {
  InputMax,
  InputNumberRequired,
  InputRequired,
  InputTextAreaMax,
} from "@common/formConfig";
import { Button, Card, Col, DatePicker, Form, Input, Row, Space } from "antd";
import dayjs from "dayjs";
import moment from "moment";
import { useEffect, useState } from "react";
import { FaCheckCircle } from "react-icons/fa";
import { IoCaretBackOutline } from "react-icons/io5";
import A0502Domain from "../domains/A0502Domain";

export default function A0502Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0502Domain();
  const { clientDetail, mode } = context || {};
  const [updateMode, setUpdateMode] = useState(true);

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    if (mode == "update") {
      form.setFieldsValue({
        firstName: clientDetail?.firstName,
        lastName: clientDetail?.lastName,
        phoneNumber: clientDetail?.phoneNumber,
        address: clientDetail?.address,
        role: clientDetail?.role,
        dateOfBirth: dayjs("2024-04-26", "YYYY-MM-DD"),
      });
    } else {
      form.resetFields();
    }
  }, [clientDetail]);

  // useEffect(( => Ơ))

  async function createClient(values) {
    await domain.createClient(values);
  }

  async function updateClient() {
    await form.validateFields();
    let values = await form.getFieldsValue(true);
    await domain.updateClient(values);
  }

  return (
    <div className="h-auto w-auto">
      <Card
        title={
          mode == "update"
            ? "Chỉnh sửa thông tin khách hàng"
            : "Tạo mới thông tin khách hàng"
        }
        className="w-full"
      >
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          initialValues={{
            phoneNumber: [""],
            address: [""],
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
          onFinish={createClient}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Họ"
              name="firstName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Tên"
              name="lastName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Ngày sinh"
              name="dateOfBirth"
              rules={[...InputNumberRequired]}
              className="mt-2-custom"
            >
              <DatePicker
                placeholder="dd/mm/yyyy"
                format={"DD/MM/YYYY"}
                disabled={mode != "create" && updateMode}
                disabledDate={(current) => current && current > moment()}
              />
            </Form.Item>

            <Form.Item label="Số điện thoại" required>
              <Form.List name="phoneNumber">
                {(fields, { add, remove }, { errors }) => (
                  <>
                    {fields.map((field, index) => (
                      <Row className="mb-2">
                        <Col span={20}>
                          <Form.Item
                            {...field}
                            validateTrigger={["onChange", "onBlur"]}
                            rules={[...InputRequired]}
                          >
                            <Input
                              placeholder="Nhập thông tin"
                              disabled={mode != "create" && updateMode}
                            />
                          </Form.Item>
                        </Col>
                        <Col
                          span={4}
                          className="justyfy-center flex items-center"
                        >
                          <PlusCircleOutlined
                            onClick={() => {
                              if (mode == "create" || !updateMode) {
                                add();
                              }
                            }}
                            className="mx-1 text-2xl text-green-500"
                          />
                          {fields?.length > 1 && (
                            <MinusCircleOutlined
                              onClick={() => {
                                if (mode == "create" || !updateMode) {
                                  remove(field.name);
                                }
                              }}
                              className="mx-1 text-2xl text-red-500"
                            />
                          )}
                        </Col>
                      </Row>
                    ))}
                  </>
                )}
              </Form.List>
            </Form.Item>

            <Form.Item label="Địa chỉ" required>
              <Form.List name="address">
                {(fields, { add, remove }, { errors }) => (
                  <>
                    {fields.map((field, index) => (
                      <Row className="mb-2">
                        <Col span={20}>
                          <Form.Item
                            {...field}
                            validateTrigger={["onChange", "onBlur"]}
                            rules={[...InputRequired, ...InputTextAreaMax]}
                          >
                            <Input.TextArea
                              placeholder="Nhập thông tin"
                              disabled={mode != "create" && updateMode}
                            />
                          </Form.Item>
                        </Col>
                        <Col
                          span={4}
                          className="justyfy-center flex items-center"
                        >
                          <PlusCircleOutlined
                            onClick={() => {
                              if (mode == "create" || !updateMode) {
                                add();
                              }
                            }}
                            className="mx-1 text-2xl text-green-500"
                          />
                          {fields?.length > 1 && (
                            <MinusCircleOutlined
                              onClick={() => {
                                if (mode == "create" || !updateMode) {
                                  remove(field.name);
                                }
                              }}
                              className="mx-1 text-2xl text-red-500"
                            />
                          )}
                        </Col>
                      </Row>
                    ))}
                  </>
                )}
              </Form.List>
            </Form.Item>

            <Form.Item label="Ghi chú" name="note" className="mt-2-custom">
              <Input.TextArea
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>

            {mode == "create" && (
              <Form.Item label=" ">
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-blue-6 text-white"
                  icon={<FaCheckCircle />}
                >
                  Xác nhận tạo mới
                </Button>
              </Form.Item>
            )}
            {mode == "update" && (
              <>
                {updateMode ? (
                  <Form.Item label=" ">
                    <Button
                      type="primary"
                      className="bg-blue-6 text-white"
                      onClick={() => setUpdateMode(false)}
                      icon={<EditOutlined className="text-xl text-white" />}
                    >
                      Chỉnh sửa
                    </Button>
                  </Form.Item>
                ) : (
                  <Form.Item label=" " className="">
                    <Button
                      type="primary"
                      className="bg-grey-3 ml-2 text-black mr-4"
                      onClick={() => setUpdateMode(true)}
                      icon={<IoCaretBackOutline />}
                    >
                      Quay lại
                    </Button>
                    <Button
                      type="primary"
                      className="bg-blue-6 text-white"
                      onClick={() => updateClient()}
                      icon={<FaCheckCircle />}
                    >
                      Xác nhận chỉnh sửa
                    </Button>
                  </Form.Item>
                )}
              </>
            )}
          </Space>
        </Form>
      </Card>
    </div>
  );
}
