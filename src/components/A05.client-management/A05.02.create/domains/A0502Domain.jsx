import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import moment from "moment";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import A0502ContextService from "../services/A0502ContextService";

export default function A0502Domain() {
  const [context, contextService] = A0502ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();
  const mode = _.includes(location.pathname, "update") ? "update" : "create";
  const { id } = useParams();
  const contextRef = useRef({
    mode: mode,
    clientDetail: {},
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    if (!isSuperAdmin()) {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/client-management");
    } else {
      if (mode == "update") {
        await getClient(id);
      }
    }
  };

  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  async function getClient(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/crm/client/detail/` + id,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        contextRef.current.clientDetail = data || {};
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function createClient(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "post",
        url: `/api/admin/crm/client/create`,
        data: {
          firstName: params?.firstName,
          lastName: params?.lastName,
          address: params?.address,
          phoneNumber: params?.phoneNumber,
          dateOfBirth: params?.dateOfBirth
            ? moment(params?.dateOfBirth).format("DD/MM/YYYY")
            : null,
          note: params?.note,
        },
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/client-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateClient(params) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "put",
        url: `/api/admin/crm/client/edit/` + id,
        data: {
          firstName: params?.firstName,
          lastName: params?.lastName,
          address: params?.address,
          phoneNumber: params?.phoneNumber,
          dateOfBirth: params?.dateOfBirth
            ? moment(params?.dateOfBirth).format("DD/MM/YYYY")
            : null,
          note: params?.note,
        },
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/client-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }

  const domainInterface = useRef({
    initDomain,
    createClient,
    updateClient,
  });

  return [context, domainInterface.current];
}
