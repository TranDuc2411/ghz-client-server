import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0502ActionList,
  A0502Context,
  createContext,
} from '../contexts/A0502Context';

export default function A0502ContextService() {
  const context = useStore()[A0502Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0502Context,
      type: A0502ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0502Context,
      type: A0502ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
