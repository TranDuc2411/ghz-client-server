import { createSlice } from "@core/store/store";

const A0502Context = "A0502Context";
const A0502ActionList = Object.freeze({
  UpdateContext: A0502Context + "/update",
  ResetContext: A0502Context + "/reset",
});

const A0502InitalState = {};

const A0502Actions = {};

A0502Actions[A0502ActionList.UpdateContext] = (state, payload) => {
  if (A0502Context != payload?.slice) {
    return state;
  }
  return { ...state, A0502Context: payload.data };
};

A0502Actions[A0502ActionList.ResetContext] = (state, payload) => {
  if (A0502Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0502InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0502Context, A0502Actions, {
    ...A0502InitalState,
    ...data,
  });
};
export { A0502ActionList, A0502Actions, A0502Context, createContext };
