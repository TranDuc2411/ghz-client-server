import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Drawer,
  Form,
  Input,
  InputNumber,
  Radio,
  Row,
  Space,
} from "antd";
const { RangePicker } = DatePicker;
export default function A0601Filter({ filter, setFilter }) {
  return (
    <Drawer
      title="Bộ lọc"
      onClose={() => setFilter(false)}
      open={filter}
      extra={
        <Space>
          <Button>Làm mới</Button>
          <Button className="bg-blue-6 text-white">Tìm kiếm</Button>
        </Space>
      }
    >
      <Form
        layout="vertical"
        initialValues={{
          isDeleted: [1],
        }}
      >
        <Row gutter={16}>
          <Col span={24} className="mb-5">
            <Form.Item name="isDeleted" label="">
              <Checkbox.Group>
                <Checkbox value={1}>Chưa bán</Checkbox>
                <Checkbox value={2}>Đã bán</Checkbox>
              </Checkbox.Group>
            </Form.Item>
          </Col>

          <Col span={24} className="mb-2">
            <Form.Item name="name" label="Tên phụ kiện">
              <Input placeholder="Nhập thông tin" />
            </Form.Item>
          </Col>

          <Col span={24} className="my-4 flex">
            <Form.Item name="" label="Giá nhập">
              <div>
                <div className="flex">
                  <Form.Item className="mx-2">Từ</Form.Item>
                  <Form.Item name="status">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập từ"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                  <Form.Item className="mx-2">Đến</Form.Item>
                  <Form.Item name="status">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập đến"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                </div>
                <div className="mt-2 ml-2">
                  <Radio.Group>
                    <Radio value={1}>Tăng</Radio>
                    <Radio value={2}>Giảm</Radio>
                  </Radio.Group>
                </div>
              </div>
            </Form.Item>
          </Col>

          <Col span={24} className="my-4 flex">
            <Form.Item name="" label="Giá bán đề xuất">
              <div>
                <div className="flex">
                  <Form.Item className="mx-2">Từ</Form.Item>
                  <Form.Item name="status">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập từ"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                  <Form.Item className="mx-2">Đến</Form.Item>
                  <Form.Item name="status">
                    <InputNumber
                      mode="multiple"
                      placeholder="Giá nhập đến"
                      className="w-[120px]"
                      min={0}
                      max={10000000000000}
                      formatter={(value) =>
                        `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                      controls={false}
                    />
                  </Form.Item>
                </div>
                <div className="mt-2 ml-2">
                  <Radio.Group>
                    <Radio value={1}>Tăng</Radio>
                    <Radio value={2}>Giảm</Radio>
                  </Radio.Group>
                </div>
              </div>
            </Form.Item>
          </Col>

          <Col span={24} className="my-2">
            <Form.Item name="state" label="Ngày tạo">
              <RangePicker placeholder={["Ngày từ", "Ngày đến"]} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
}
