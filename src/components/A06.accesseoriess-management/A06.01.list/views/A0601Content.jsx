import { FileSearchOutlined, PlusOutlined } from "@ant-design/icons";
import {
  configBgColor,
  formatCurrency,
  getScreenWidth,
  isSuperAdmin,
} from "@common/utils";
import { Button, Card, Input, Table } from "antd";
import _ from "lodash";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import A0601Domain from "../domains/A0601Domain";
import A0601Filter from "./A0601Filter";

export default function A0601Content() {
  const [context, domain] = A0601Domain();
  const [filter, setFilter] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    setDataSource(context?.dataTable);
  }, [context?.dataTable]);

  function getColumns() {
    let result = [
      {
        title: "Stt",
        dataIndex: "index",
        key: "index",
        fixed: "left",
        width: 50,
        render: (_value, _record, index) => index + 1,
      },
      {
        title: "Tên phụ kiện",
        dataIndex: "accessoryName",
        key: "accessoryName",
        width: 100,
      },
    ];
    if (isSuperAdmin()) {
      result = [
        ...result,
        ...[
          {
            title: "Giá nhập",
            dataIndex: "purchasePrice",
            key: "purchasePrice",
            width: 100,
            render: (value) => value && formatCurrency(value),
          },
        ],
      ];
    }

    result = [
      ...result,
      ...[
        {
          title: "Giá bán đề xuất",
          dataIndex: "sellingPrice",
          key: "sellingPrice",
          width: 100,
          render: (value) => value && formatCurrency(value),
        },
      ],
    ];
    result = [
      ...result,
      ...[
        {
          title: "Nhà cung cấp",
          dataIndex: "supplier",
          key: "supplier",
          width: 100,
        },
      ],
    ];
    return result || [];
  }
  return (
    <div className="h-auto w-auto">
      <A0601Filter filter={filter} setFilter={setFilter} />

      <Card
        className="w-auto"
        title={
          <span className={`break-words`}>Danh sách phụ kiện trong kho</span>
        }
        extra={
          <div className="flex flex-wrap max-w-[400px]">
            <Input
              placeholder="Tìm kiếm"
              className="ml-2 w-[150px]"
              onChange={async (e) => {
                const value = e.target.value.toLowerCase().trim();
                let result = _.filter(context.dataTable, (el) =>
                  el?.accessoryName?.toLowerCase().trim().includes(value)
                );
                setDataSource(result);
              }}
            />
            <Button
              className="bg-grey-3 ml-2"
              onClick={() => setFilter(true)}
              icon={<FileSearchOutlined />}
            >
              {getScreenWidth() == "pc" && "Bộ lọc"}
            </Button>
            {isSuperAdmin() && (
              <Button
                className="bg-blue-6 text-white ml-2"
                onClick={() => navigate("/accesseoriess-management/create")}
                icon={<PlusOutlined />}
              >
                {getScreenWidth() == "pc" && "Tạo mới"}
              </Button>
            )}
          </div>
        }
      >
        <Table
          className="w-full h-90"
          dataSource={dataSource}
          bordered
          size="small"
          rowClassName={(record) => `${configBgColor(record?.state?.color)}`}
          scroll={{
            // x: "calc(700px + 50%)",
            y: "auto",
          }}
          footer={null}
          columns={getColumns()}
        />
      </Card>
    </div>
  );
}
