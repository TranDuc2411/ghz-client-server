import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import A0601ContextService from "../services/A0601ContextService";

export default function A0601Domain() {
  const [context, contextService] = A0601ContextService();
  const common = UseCommon();
  const api = useAxiosAPI();
  const contextRef = useRef({
    dataTable: [],
  });

  const initDomain = async () => {
    await contextService.initContext(contextRef.current);
    await getAllAccesseoriess();
  };

  async function getAllAccesseoriess() {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/warehouse/accessory/getall`,
        data: {
          isDeleted: true,
        },
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        const res = _.map(data, (el, index) => ({
          ...el,
          key: index, // thêm ký cho mảng để không bị lỗi
        }));
        contextRef.current.dataTable = res || [];
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
  });

  return [context, domainInterface.current];
}
