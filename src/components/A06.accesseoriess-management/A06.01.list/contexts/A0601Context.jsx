import { createSlice } from "@core/store/store";

const A0601Context = "A0601Context";
const A0601ActionList = Object.freeze({
  UpdateContext: A0601Context + "/update",
  ResetContext: A0601Context + "/reset",
});

const A0601InitalState = {};

const A0601Actions = {};

A0601Actions[A0601ActionList.UpdateContext] = (state, payload) => {
  if (A0601Context != payload?.slice) {
    return state;
  }
  return { ...state, A0601Context: payload.data };
};

A0601Actions[A0601ActionList.ResetContext] = (state, payload) => {
  if (A0601Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0601InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0601Context, A0601Actions, {
    ...A0601InitalState,
    ...data,
  });
};
export { A0601ActionList, A0601Actions, A0601Context, createContext };
