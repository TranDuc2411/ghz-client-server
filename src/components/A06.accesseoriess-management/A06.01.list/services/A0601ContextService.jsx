import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0601ActionList,
  A0601Context,
  createContext,
} from '../contexts/A0601Context';

export default function A0601ContextService() {
  const context = useStore()[A0601Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0601Context,
      type: A0601ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0601Context,
      type: A0601ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
