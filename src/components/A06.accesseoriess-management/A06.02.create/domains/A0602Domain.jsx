import { isSuperAdmin } from "@common/utils";
import useAxiosAPI from "@core/hooks/UseAxiosApi";
import UseCommon from "@core/hooks/UseCommon";
import { message } from "antd";
import _ from "lodash";
import { useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import A0602ContextService from "../services/A0602ContextService";

export default function A0602Domain() {
  const [context, contextService] = A0602ContextService();
  const common = UseCommon();
  const navigate = useNavigate();
  const api = useAxiosAPI();
  const mode = _.includes(location.pathname, "update") ? "update" : "create";
  const { id } = useParams();

  const initContext = {
    mode: mode,
    statusOption: [],
    stickerOption: [],
    accesseoriessNameOption: [],
    accesseoriessDetail: {},
  };
  const contextRef = useRef(initContext);
  const initDomain = async () => {
    await contextService.initContext(initContext);

    if (!isSuperAdmin() && mode == "create") {
      message.error("Tài khoản không có quyền truy cập");
      navigate("/accesseoriess-management");
    } else {
      if (mode == "update") {
        await getAccesseoriess(id);
      }
    }
  };

  //-----------------------------------------
  async function getAccesseoriess(id) {
    try {
      common.backdrop(true);
      let response = await api({
        method: "get",
        url: `/api/admin/warehouse/accessory/detail/${id}`,
      });
      const { data, status } = response.data || {};
      if (status == 200 && !!data) {
        contextRef.current.accesseoriessDetail = data || {};
        contextService.updateContext(contextRef.current);
      } else {
        message.error(response.data.message);
      }
    } catch (error) {
      console.log("error", error);
    } finally {
      common.backdrop(false);
    }
  }
  async function createAccesseoriess(params) {
    try {
      common.backdrop(true);
      let accesseoriessRequest = {
        accessoryName: params?.accessoryName,
        sellingPrice: params?.sellingPrice,
        purchasePrice: params?.purchasePrice,
        quantity: params?.quantity,
        supplier: params?.supplier,
      };
      let response = await api({
        method: "post",
        url: `/api/supper-admin/warehouses/accesseoriess/create`,
        data: accesseoriessRequest,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/accesseoriess-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  async function updateAccesseoriess(params) {
    try {
      common.backdrop(true);
      let accesseoriessRequest = {
        accessoryName: params?.accessoryName,
        sellingPrice: params?.sellingPrice,
        purchasePrice: params?.purchasePrice,
        quantity: params?.quantity,
        supplier: params?.supplier,
      };
      let response = await api({
        method: "put",
        url: `/api/supper-admin/warehouses/accesseoriess/update/` + id,
        data: accesseoriessRequest,
      });
      const { status } = response.data || {};
      if (status == 200) {
        message.success(response.data.message);
        navigate("/accesseoriess-management");
      } else {
        message.error(response.data.message);
      }
    } finally {
      common.backdrop(false);
    }
  }
  /*---------------------------------------------------------------- */
  /** Handle API and update context*/
  /*---------------------------------------------------------------- */
  const domainInterface = useRef({
    initDomain,
    createAccesseoriess,
    updateAccesseoriess,
  });

  return [context, domainInterface.current];
}
