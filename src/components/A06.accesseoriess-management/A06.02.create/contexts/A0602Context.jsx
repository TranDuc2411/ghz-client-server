import { createSlice } from "@core/store/store";

const A0602Context = "A0602Context";
const A0602ActionList = Object.freeze({
  UpdateContext: A0602Context + "/update",
  ResetContext: A0602Context + "/reset",
});

const A0602InitalState = {};

const A0602Actions = {};

A0602Actions[A0602ActionList.UpdateContext] = (state, payload) => {
  if (A0602Context != payload?.slice) {
    return state;
  }
  return { ...state, A0602Context: payload.data };
};

A0602Actions[A0602ActionList.ResetContext] = (state, payload) => {
  if (A0602Context != payload?.slice) {
    return state;
  }
  return { ...state, ...A0602InitalState };
};

const createContext = (dispatcher, data) => {
  createSlice(dispatcher, A0602Context, A0602Actions, {
    ...A0602InitalState,
    ...data,
  });
};
export { A0602ActionList, A0602Actions, A0602Context, createContext };
