import { useDispatch, useStore } from '@core/store/store';
import { useRef } from 'react';
import {
  A0602ActionList,
  A0602Context,
  createContext,
} from '../contexts/A0602Context';

export default function A0602ContextService() {
  const context = useStore()[A0602Context];
  const dispatcher = useDispatch();

  const initContext = (data) => {
    createContext(dispatcher, data);
  };

  const updateContext = (data) => {
    dispatcher({
      slice: A0602Context,
      type: A0602ActionList.UpdateContext,
      data: data,
    });
  };

  const resetContext = () => {
    dispatcher({
      slice: A0602Context,
      type: A0602ActionList.ResetContext,
    });
  };

  const dispatchInterface = useRef({
    initContext,
    updateContext,
    resetContext,
  });

  return [context, dispatchInterface.current];
}
