import { EditOutlined } from "@ant-design/icons";
import {
  InputMax,
  InputNumberRequired,
  InputRequired,
} from "@common/formConfig";
import { isSuperAdmin } from "@common/utils";
import { Button, Card, Form, Input, InputNumber, Space } from "antd";
import moment from "moment/moment";
import { useEffect, useState } from "react";
import { FaCheckCircle } from "react-icons/fa";
import { IoCaretBackOutline } from "react-icons/io5";
import A0602Domain from "../domains/A0602Domain";

export default function A0602Content() {
  const [form] = Form.useForm();
  const [context, domain] = A0602Domain();
  const { accesseoriessDetail, mode } = context || {};
  const [updateMode, setUpdateMode] = useState(true);

  useEffect(() => {
    domain.initDomain();
  }, []);

  useEffect(() => {
    if (mode == "update") {
      form.setFieldsValue({
        accessoryName: accesseoriessDetail?.accessoryName,
        sellingPrice: accesseoriessDetail?.sellingPrice,
        purchasePrice: accesseoriessDetail?.purchasePrice,
        supplier: accesseoriessDetail?.supplier,
        quantity: accesseoriessDetail?.quantity,
        createdDate: moment(accesseoriessDetail?.createdDate).format(
          "DD/MM/YYYY"
        ),
      });
    }
  }, [accesseoriessDetail]);

  async function createAccesseoriess(values) {
    await domain.createAccesseoriess(values);
  }

  async function updateAccesseoriess() {
    await form.validateFields();
    let values = await form.getFieldsValue(true);
    await domain.updateAccesseoriess(values);
  }

  return (
    <div className="h-auto w-auto">
      <Card
        title={mode == "update" ? "Chỉnh sửa phụ kiện" : "Tạo mới phụ kiện"}
        className="w-full"
        extra={
          <Button danger onClick={() => setUpdateMode(false)}>
            Nhập lại phụ kiện
          </Button>
        }
      >
        <Form
          className="w-full"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{ flex: 1, span: 16 }}
          colon={false}
          style={{ maxWidth: 600 }}
          onFinish={createAccesseoriess}
        >
          <Space direction="vertical" className="w-full">
            <Form.Item
              label="Tên phụ kiện"
              name="accessoryName"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Số lượng"
              name="quantity"
              rules={[...InputNumberRequired]}
              className="mt-2-custom"
            >
              <InputNumber
                placeholder="Nhập thông tin"
                className="w-[200px]"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            <Form.Item
              label="Giá nhập"
              name="sellingPrice"
              rules={[...InputNumberRequired]}
              className="mt-2-custom"
            >
              <InputNumber
                placeholder="Nhập thông tin"
                className="w-[200px]"
                min={0}
                max={10000000000000}
                formatter={(value) =>
                  `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                }
                parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                controls={false}
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>
            {isSuperAdmin() && (
              <Form.Item
                label="Giá bán đề xuất"
                name="purchasePrice"
                rules={[...InputNumberRequired]}
                className="mt-2-custom"
              >
                <InputNumber
                  placeholder="Nhập thông tin"
                  className="w-[200px]"
                  min={0}
                  max={10000000000000}
                  controls={false}
                  formatter={(value) =>
                    `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                  }
                  parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
                  disabled={mode != "create" && updateMode}
                />
              </Form.Item>
            )}
            {mode !== "create" && (
              <Form.Item
                label="Ngày tạo"
                name="createdDate"
                className="mt-2-custom"
              >
                <Input disabled={true} />
              </Form.Item>
            )}
            <Form.Item
              label="Nhà cung cấp"
              name="supplier"
              rules={[...InputRequired, ...InputMax]}
              className="mt-2-custom"
            >
              <Input
                placeholder="Nhập thông tin"
                disabled={mode != "create" && updateMode}
              />
            </Form.Item>

            {isSuperAdmin() && (
              <>
                {mode == "create" && (
                  <Form.Item label=" ">
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="bg-blue-6 text-white"
                      icon={<FaCheckCircle />}
                    >
                      Xác nhận tạo mới
                    </Button>
                  </Form.Item>
                )}
                {mode == "update" && (
                  <>
                    {updateMode ? (
                      <Form.Item label=" ">
                        <Button
                          type="primary"
                          className="bg-blue-6 text-white"
                          onClick={() => setUpdateMode(false)}
                          icon={<EditOutlined className="text-xl text-white" />}
                        >
                          Chỉnh sửa
                        </Button>
                      </Form.Item>
                    ) : (
                      <Form.Item label=" " className="">
                        <Button
                          type="primary"
                          className="bg-grey-3 ml-2 text-black mr-4"
                          onClick={() => setUpdateMode(true)}
                          icon={<IoCaretBackOutline />}
                        >
                          Quay lại
                        </Button>
                        <Button
                          type="primary"
                          className="bg-blue-6 text-white"
                          onClick={() => updateAccesseoriess()}
                          icon={<FaCheckCircle />}
                        >
                          Xác nhận chỉnh sửa
                        </Button>
                      </Form.Item>
                    )}
                  </>
                )}
              </>
            )}
          </Space>
        </Form>
      </Card>
    </div>
  );
}
