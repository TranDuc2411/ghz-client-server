import { Button, Modal } from "antd";
import { useState } from "react";
import { BlockPicker } from "react-color";

export default function ColorPickerModal({ openColorPicker, onCancel, onOk }) {
  const [color, setColor] = useState("#5DA0F6");
  return (
    <Modal
      open={openColorPicker}
      width={320}
      onCancel={onCancel}
      closable={false}
      footer={[
        <Button key={1} onClick={onCancel}>
          Hủy
        </Button>,
        <Button
          key={2}
          className="bg-blue-6 text-white ml-2"
          onClick={() => {
            onOk(color);
          }}
        >
          Xác nhận
        </Button>,
      ]}
    >
      <div className="h-full w-full justify-center items-center bg-red-4 flex">
        <BlockPicker
          className="shadow-md w-full"
          color={color}
          onChangeComplete={(e) => {
            setColor(e.hex);
          }}
          colors={[
            "#5DA0F6", // bg-blue-5
            "#FFA940", // bg-orange-5
            "#FF4D4F", // bg-red-5
            "#4DD077", // bg-green-5
            "#BFBFBF", // bg-grey-6
            "#36CFC9", // bg-cyan-5
            "#FFD700", // bg-yellow-1
            "#EE82EE", // bg-violet-1
          ]}
        />
      </div>
    </Modal>
  );
}
