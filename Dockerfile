# Sử dụng Nginx làm base image
FROM nginx

# Copy các file từ thư mục dist vào thư mục root của Nginx
COPY dist/ /usr/share/nginx/html/

# Xoá default.conf của Nginx mặc định
RUN rm /etc/nginx/conf.d/default.conf

# Copy nginx.conf đã cấu hình vào container
COPY nginx.conf /etc/nginx/conf.d/

# Expose cổng 80 của container để có thể truy cập vào Nginx
EXPOSE 80

# CMD để chạy lệnh khi container được khởi chạy (không cần thiết với Nginx, vì Nginx sẽ tự động khởi động)
# CMD ["nginx", "-g", "daemon off;"]
