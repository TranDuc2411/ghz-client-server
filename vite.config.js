import react from "@vitejs/plugin-react-swc";
import path from "path";
import { defineConfig } from "vite";
import envCompatible from 'vite-plugin-env-compatible';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), envCompatible()],
  // server: {
  //   port: 80,
  // },
  resolve: {
    // cấu hình đường dẫn vào thu mục
    alias: {
      "@components": path.resolve(__dirname, "src/components/"),
      "@container": path.resolve(__dirname, "src/container/"),
      "@assets": path.resolve(__dirname, "src/assets/"),
      "@layout": path.resolve(__dirname, "src/layout/"),
      "@common": path.resolve(__dirname, "src/common/"),
      "@core": path.resolve(__dirname, "src/core/"),
    },
  },
  css: {
    preprocessorOptions: {
      less: {
        modifyVars: {
          "@primary-color": "#0043A5",
          "@secondary-color": "#00CCD6",
          "@success-color": "#4DD077",
          "@warning-color": "#FF7A45",
          "@error-color": "#FF4D4F",
          "@menu-dark-item-active-bg": "#00ccd6",
          "@text-color": "#262626",
          "@form-item-margin-bottom": "16px",
          "@border-radius-base": "5px",
          "@table-border-color": "#e8e8e8",
        },
        javascriptEnabled: true,
      },
    },
  },
});
